ZipStates
=========
This a module written in PHP 5.3.1+ (and, therefore, requires Namespaces support) which will take
a country code (the ISO 3166 code such as "US" for the United States of America, "GB" for Great Britain,
and "AU" for Australia) and a zip/postal code and allow you to perform a number of functions on it.

What will it allow you to do:

    /**
     * Validates a postal code
     *
     * If the postal/zipcode is "obviously" incorrect for the country (for example, letters in an
     * American postal code, or "FRED" for a GB one), then this will return FALSE. If it does "look"
     * valid, it will format it to the expected standard for that country (so "le93bh" in GB
     * will return 'LE9 3BH'). If we can't deal with the country yet, NULL is returned (as it is neither
     * valid or invalid)
     *
     * @param string $country The two character country code
     * @param string $zipcode The postal/zip code
     * @return bool|null|string NULL if unable to valid, FALSE if incorrect, string if postal code
     */
    $zipstate->validateZipcode($country, $zipcode);

    /**
     * Gets the appropriate state/region subdivision by an zipcode
     *
     * @param string $country The two character country code
     * @param string $zipcode The postal/zip code
     * @return string|null The subdivision or NULL if unable to match
     */
    $zipstate->getStateSubdivisionByZipcode($country, $zipcode);

     /**
     * Gets the appropriate country subdivision by an zipcode
     *
     * Only really useful for GB which is made up of multiple constituent parts, but some
     * zip codes (such as the 00601 US zip code which is actually Puerto Rico) map to other countries
     *
     * @param string $country The two character country code
     * @param string $zipcode The postal/zip code
     * @return string|null The subdivision or NULL if unable to match
     */
    $zipstate->getCountrySubdivisionByZipcode($country, $zipcode)

Copyright
=========
This module was originally written by Richard Chiswell of Bairwell Ltd. Whilst Bairwell Ltd
holds the copyright on this work, we have licenced it under Creative Commons.

Bairwell Ltd: http://www.bairwell.com / Twitter: http://twitter.com/bairwell
Richard Chiswell: http://blog.rac.me.uk / Twitter: http://twitter.com/rchiswell

Licence
=======
This work is licensed under the Creative Commons Attribution 3.0 Unported License.
To view a copy of this license, visit http://creativecommons.org/licenses/by/3.0/ or send
a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.

This work is based on information available via Wikipedia and other sources.

Demonstrations
==============
To see a demonstration of the code:

php Bairwell/zipstatesdemo.php

expect it to return output in the format:

The postal/zip code of LE9 3BH in GB maps to the area GB-LCE in GB-ENG
The postal/zip code of WC1E 7HJ in GB maps to the area GB-LND in GB-ENG
The postal/zip code of TN23 4GL in GB maps to the area GB-KEN in GB-ENG
The postal/zip code of 90210 in US maps to the area US-CA in US

Unit tests
==========
This code comes complete with phpUnit compatible unit tests under the "Tests" subdirectory, but
due to the different postcodes which need to be checked, there are 16 different tests with a
total of 87,254 assertions and takes around 30 seconds to run (depending on your hardware)

phpunit -config Bairwell/phpunit.xml Bairwell/Tests/ZipStates/

Building
========
There is an Ant/Jenkins compatible build.xml file within Tests/ which checks the unit tests,
runs PHP_Depend, PHPMD, PHPCPD, PHPLOC, PHP_CodeSniffer, PHP_CodeBrowser, and DocBlox. It takes
about 12.5 minutes to build on an Amazon EC2 Micro instance.