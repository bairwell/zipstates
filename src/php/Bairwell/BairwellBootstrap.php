<?php
/**
 * This is the main bootstrap used by Bairwell items.
 *
 * This work is licensed under the Creative Commons Attribution 3.0 Unported License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/3.0/ or send
 * a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 *
 * @package Bairwell
 * @subpackage ZipStates
 * @author Richard Chiswell <richard@Bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license Creative Commons Attribution 3.0 Unported License
 */

/**
 * PHP_MAJOR_VERSION isn't defined until 5.2.7, so if it isn't defined, we know we're running on an old version
 */
if (defined('PHP_MAJOR_VERSION') === FALSE) {
    die('We require PHP 5.3.0 at a minimum as we use namespaces and late static bindings. You have PHP ' . phpversion());
}
if (PHP_MAJOR_VERSION === 5 && PHP_MINOR_VERSION < 3) {
    die('We require PHP 5.3.0 at a minimum as we use namespaces and late static bindings. You have PHP ' . phpversion());
}
if (defined('MB_OVERLOAD_STRING') === FALSE) {
    die('We require that PHP has mbstring installed: http://php.net/mbstring.installation.php');
}


if (defined('Bairwell_APP_PATH') === FALSE) {
    /**
     * Set the path all the Bairwell applications can expect to find their files under
     */
    define('Bairwell_APP_PATH', dirname(__FILE__) . '/..');
    set_include_path('.' . PATH_SEPARATOR . get_include_path());

}

/**
 * We use UTF-8 encoding
 */
mb_internal_encoding('UTF-8');


/**
 * The autoloader.
 *
 * Included in the bootstrap as otherwise it's difficult to load it.
 *
 * @throws Exception
 */
class BairwellBootstrap
{
    /**
     * Autoloads an appropriate class.
     *
     * @throws \Exception If there is a problem loading the class
     * @param string $className The name of the class we are trying to load
     * @return void
     */
    public static function autoloader($className)
    {
        $namespaceSeparator = '\\';
        $filename = Bairwell_APP_PATH . DIRECTORY_SEPARATOR . str_replace($namespaceSeparator, DIRECTORY_SEPARATOR, $className) . '.php';
        $realpath = realpath($filename);
        if ($realpath === FALSE) {
            throw new \Exception('Filepath for ' . $className . ' unable to be mapped to realpath: ' . $filename);
        }
        include $filename;
        if (class_exists($className) === FALSE && interface_exists($className) === FALSE) {
            throw new \Exception('Unable to load class ' . $className . ' from ' . $filename);
        }
    }
}

/**
 * Register our autoloader
 */
spl_autoload_register('BairwellBootstrap::autoloader');
