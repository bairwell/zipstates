<?php
/**
 * This is the main Factory class used by Bairwell items.
 *
 * This work is licensed under the Creative Commons Attribution 3.0 Unported License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/3.0/ or send
 * a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 *
 * @package Bairwell
 * @subpackage ZipStates
 * @author Richard Chiswell <richard@Bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license Creative Commons Attribution 3.0 Unported License
 */
namespace Bairwell;

/**
 * A basic factory class to enable Unittesting in the future
 */
abstract class Factory
{

    /**
     * Returns the class prefix for this factory class
     *
     * Should be set by the individual factories to return something like
     *  NameAlternatives
     *
     * @static
     * @abstract
     * @return string
     */
    abstract protected static function getClassPrefix();

    /**
     * This holds the created instances of objects
     *
     * @var array
     */
    protected static $objects;

    /**
     * Cannot call constructor from outside the class.
     * Helps enforce singletons
     *
     */
    protected function __construct()
    {
    }

    /**
     * Should we be enforcing mock objects only?
     *
     * @var bool
     */
    protected static $mocksOnly = FALSE;

    /**
     * Returns an appropriate class/object
     *
     * If mocksOnly is enabled, then only Mocks or Closures will be returned (ideal for unit testing)
     *
     * @static
     * @param string $name The name of the class we are after
     * @return object The created object
     */
    public static function getLibrary($name)
    {
        $classname = '\Bairwell\\' . static::getClassPrefix() . '\\' . $name;
        if (static::$mocksOnly === TRUE) {
            if (isset(static::$objects[$classname]) === TRUE) {
                if (static::$objects[$classname] instanceof \Closure
                    || static::$objects[$classname] instanceof \PHPUnit_Framework_MockObject_MockObject
                ) {
                    return static::$objects[$classname];
                } else {
                    throw new \Exception(
                        'Requested ' . $classname . ', but that is not a Closure or ' .
                        'PHPUnit_Framework_MockObject_MockObject in "mocksOnly" mode'
                    );
                }
            } else {
                throw new \Exception(
                    'Requested ' . $classname . ', but that is not a defined Closure or ' .
                    'PHPUnit_Framework_MockObject_MockObject in "mocksOnly" mode'
                );
            }
        }
        if (isset(static::$objects[$classname]) === TRUE) {
            return static::$objects[$classname];
        }
        static::$objects[$classname] = new $classname;
        return static::$objects[$classname];

    }

    /**
     * Should we be enforcing mocks only?
     *
     * @static
     * @param bool $setting TRUE to enforce Mocks only, FALSE to allow all types of objects
     * @return void
     */
    public static function mocksOnly($setting)
    {
        static::$mocksOnly = $setting;
    }

    /**
     * Adds a class to our cache - ideal for unit testing and mocks
     *
     * @static
     * @throws \Exception
     * @param string $name The name of the class we are adding
     * @param object $mock The mock object or similar we are adding to the cache
     * @return void
     */
    public static function add($name, $mock)
    {
        $classname = '\Bairwell\\' . static::getClassPrefix() . '\\' . $name;
        if (isset(static::$objects[$classname]) === TRUE) {
            throw new \Exception('Class ' . $classname . ' already exists: cannot add to factory');
        }
        static::$objects[$classname] = $mock;
    }

    /**
     * Forget/delete an object we should know about (ideal for removing mocks)
     *
     * @static
     * @throws \Exception
     * @param string $name The class to forget about
     * @return void
     */
    public static function delete($name)
    {
        $classname = '\Bairwell\\' . static::getClassPrefix() . '\\' . $name;
        if (isset(static::$objects[$classname]) === FALSE) {
            throw new \Exception('Class ' . $classname . ' does not exist: the factory cannot delete it');
        }
        unset(static::$objects[$classname]);
    }

    /**
     * Resets all the objects we know about and disabled MocksOnly
     *
     * @static
     * @return void
     */
    public static function reset()
    {
        static::$objects = array();
        static::$mocksOnly = FALSE;
    }


}
