<?php
/**
 * The Australian class.
 *
 * This work is licensed under the Creative Commons Attribution 3.0 Unported License.
 * To view a copy of this license, visit
 * http://creativecommons.org/licenses/by/3.0/or send
 * a letter to
 * Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 *
 * The postal/zip codes look up in this file are based off the
 * following Wikipedia article
 * http://en.wikipedia.org/wiki/Postcodes_in_Australia
 * and is covered under the Creative Commons Attribution 3.0 Unported License .
 *
 * @package Bairwell
 * @subpackage ZipStates
 * @author Richard Chiswell <richard@Bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license Creative Commons Attribution 3.0 Unported License
 */
namespace Bairwell\ZipStates\Countries;

/**
 * Australia
 */
class AU extends Base
{

    /**
     * Attempts to validate a zip/postal code sent to it
     *
     * @param string $zipcode The zip code being provided
     * @return boolean|string string=formatted zipcode,False=not matched,null=unknown
     */
    public function validateZipcode($zipcode)
    {
        $noSpaces = strtoupper(preg_replace('/[ \,\.\-]/', '', $zipcode));
        $return = FALSE;
        if (preg_match('/^(NSW|ACT|VIC|QLD|SA|WA|TAS|NT)([0-9][0-9][0-9][0-9])$/', $noSpaces, $matches) === 1) {
            $return = $matches[2];
        } else if (preg_match('/^([0-9][0-9][0-9][0-9])$/', $noSpaces, $matches) === 1) {
            $return = $matches[1];
        }
        return $return;
    }

    /**
     * Gets the ISO 3166-2 state subdivision for this country from the zip/postal code
     *
     * May return:
     *  NULL : Means invalid zip/postal code
     *  xx: Unable to match subdivision of country xx
     *  xx-YYY: The country XX and subdivision YYY
     *
     * @throws \Exception If type is not recognised
     * @param string $zip The zipcode/postal code
     * @return string|null NULL=Invalid zipcode for area,string=as much of the subdivision as possible (including the country code)
     */
    public function getStateSubdivisionFromZipcode($zip)
    {
        $zip = $this->validateZipcode($zip);
        if (is_string($zip) === FALSE) {
            return NULL;
        }
        $zip = trim(preg_replace('/[^0-9]/', '', $zip)); // only interested in digits
        $numbers = (int)mb_substr($zip, 0, 4);
        /**
         * Normal items
         *
         * Data from:
         * http://en.wikipedia.org/wiki/Postcodes_in_Australia
         */
        $ranges = array(
            array(800, 999, 'AU-NT'),
            array(200, 299, 'AU-ACT'),
            array(1000, 2599, 'AU-NSW'),
            array(2619, 2619, 'AU-NSW'),
            array(2621, 2898, 'AU-NSW'),
            array(2899, 2899, 'NF'), // Norfolk Island
            array(2620, 2620, 'AU-ACT'),
            array(2921, 2999, 'AU-NSW'),
            array(2600, 2618, 'AU-ACT'),
            array(2900, 2920, 'AU-ACT'),
            array(3000, 3999, 'AU-VIC'),
            array(8000, 8999, 'AU-VIC'),
            array(4000, 4999, 'AU-QLD'),
            array(9000, 9999, 'AU-QLD'),
            array(5000, 5999, 'AU-SA'),
            array(6000, 6797, 'AU-WA'),
            array(6799, 6799, 'CC'), // Cocos Islands
            array(6798, 6798, 'CX'), // Christmas Island
            array(6800, 6999, 'AU-WA'),
            array(7000, 7799, 'AU-TAS'),
            array(7800, 7999, 'AU-TAS')
        );
        foreach ($ranges as $range) {
            if ($numbers >= $range[0] && $numbers <= $range[1]) {
                return $range[2];
            }
        }

        return 'AU';
    }


}
