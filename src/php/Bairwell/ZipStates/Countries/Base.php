<?php
/**
 * The base class for all of our ZipStates conversions.
 *
 * This work is licensed under the Creative Commons Attribution 3.0 Unported License.
 * To view a copy of this license, visit
 * http://creativecommons.org/licenses/by/3.0/or send
 * a letter to
 * Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 *
 * @package Bairwell
 * @subpackage ZipStates
 * @author Richard Chiswell <richard@Bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license Creative Commons Attribution 3.0 Unported License
 */
namespace Bairwell\ZipStates\Countries;

/**
 * This base class is extended by all the countries to ensure that the
 * appropriate methods are set in a standard manner
 */
abstract class Base
{

    /**
     * Attempts to validate a zip/postal code sent to it
     *
     * @param string $zipcode The zip code being provided
     * @return boolean|string string=formatted zipcode,False=not matched,null=unknown
     */
    abstract public function validateZipcode($zipcode);

    /**
     * Gets the ISO 3166-2 state subdivision for this country from the zip/postal code
     *
     * May return:
     *  NULL : Means invalid zip/postal code
     *  xx: Unable to match subdivision of country xx
     *  xx-YYY: The country XX and subdivision YYY
     *
     * @throws \Exception If type is not recognised
     * @param string $zip The zipcode/postal code
     * @return string|null NULL=Invalid zipcode for area,string=as much of the subdivision as possible (including the country code)
     */
    abstract public function getStateSubdivisionFromZipcode($zip);

    /**
     * Gets the ISO 3166-2 country subdivision for this country from the zip/postal code
     *
     * Only really relevant for the UK
     *
     * May return:
     *  NULL : Means invalid zip/postal code
     *  xx: Unable to match subdivision of country xx
     *  xx-YYY: The country XX and subdivision YYY
     *
     * @throws \Exception If type is not recognised
     * @param string $zip The zipcode/postal code
     * @return string|null NULL=Invalid zipcode for area,string=as much of the subdivision as possible (including the country code)
     */
    public function getCountrySubdivisionFromZipcode($zip)
    {
        $state = $this->getStateSubdivisionFromZipcode($zip);

        if ($state === NULL) {
            return NULL;
        }
        if (mb_strlen($state) === 2) {
            return $state;
        }
        return mb_substr($state, 0, 2);
    }


}
