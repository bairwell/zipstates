<?php
/**
 * The Canadian zipstates class.
 *
 * This work is licensed under the Creative Commons Attribution 3.0 Unported License.
 * To view a copy of this license, visit
 * http://creativecommons.org/licenses/by/3.0/or send
 * a letter to
 * Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 *
 * The postal/zip codes look ups in this file are based off the following Wikipedia article
 * http://en.wikipedia.org/wiki/Postal_codes_in_Canada
 * and is covered under the Creative Commons Attribution 3.0 Unported License .
 *
 * @package Bairwell
 * @subpackage ZipStates
 * @author Richard Chiswell <richard@Bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license Creative Commons Attribution 3.0 Unported License
 */
namespace Bairwell\ZipStates\Countries;

/**
 * Canada
 */
class CA extends Base
{

    /**
     * The list of zip code prefixes from http://en.wikipedia.org/wiki/Postal_codes_in_Canada
     * @var array The postcode prefixes to counties
     */
    private $stateszips = array(
        // A
        'A0A' => 'NL', 'A1A' => 'NL', 'A2A' => 'NL', 'A5A' => 'NL', 'A8A' => 'NL', 'A0B' => 'NL',
        'A1B' => 'NL', 'A2B' => 'NL', 'A0C' => 'NL', 'A1C' => 'NL', 'A0E' => 'NL', 'A1E' => 'NL',
        'A0G' => 'NL', 'A1G' => 'NL', 'A0H' => 'NL', 'A1H' => 'NL', 'A2H' => 'NL', 'A0J' => 'NL',
        'A0K' => 'NL', 'A1K' => 'NL', 'A0L' => 'NL', 'A1L' => 'NL', 'A0M' => 'NL', 'A1M' => 'NL',
        'A0N' => 'NL', 'A1N' => 'NL', 'A2N' => 'NL', 'A0P' => 'NL', 'A0R' => 'NL', 'A1S' => 'NL',
        'A0T' => 'NL', 'A0W' => 'NL', 'A1V' => 'NL', 'A2V' => 'NL', 'A1W' => 'NL', 'A1X' => 'NL',
        'A1Y' => 'NL',
        // B
        'B1A' => 'NS',
        'B2A' => 'NS', 'B3A' => 'NS', 'B4A' => 'NS', 'B5A' => 'NS', 'B9A' => 'NS', 'B1B' => 'NS',
        'B3B' => 'NS', 'B4B' => 'NS', 'B0C' => 'NS', 'B1C' => 'NS', 'B2C' => 'NS', 'B4C' => 'NS',
        'B0E' => 'NS', 'B1E' => 'NS', 'B2E' => 'NS', 'B3E' => 'NS', 'B4E' => 'NS', 'B1G' => 'NS',
        'B2G' => 'NS', 'B3G' => 'NS', 'B4G' => 'NS', 'B0H' => 'NS', 'B1H' => 'NS', 'B2H' => 'NS',
        'B3H' => 'NS', 'B4H' => 'NS', 'B0J' => 'NS', 'B1J' => 'NS', 'B2J' => 'NS', 'B3J' => 'NS',
        'B0K' => 'NS', 'B1K' => 'NS', 'B3K' => 'NS', 'B0L' => 'NS', 'B1L' => 'NS', 'B3L' => 'NS',
        'B6L' => 'NS', 'B0M' => 'NS', 'B1M' => 'NS', 'B3M' => 'NS', 'B0N' => 'NS', 'B1N' => 'NS',
        'B2N' => 'NS', 'B3N' => 'NS', 'B4N' => 'NS', 'B0P' => 'NS', 'B1P' => 'NS', 'B3P' => 'NS',
        'B4P' => 'NS', 'B0R' => 'NS', 'B1R' => 'NS', 'B2R' => 'NS', 'B3R' => 'NS', 'B4R' => 'NS',
        'B0S' => 'NS', 'B1S' => 'NS', 'B2S' => 'NS', 'B3S' => 'NS', 'B0T' => 'NS', 'B1T' => 'NS',
        'B2T' => 'NS', 'B3T' => 'NS', 'B0V' => 'NS', 'B1V' => 'NS', 'B2V' => 'NS', 'B3V' => 'NS',
        'B4V' => 'NS', 'B0W' => 'NS', 'B1W' => 'NS', 'B2W' => 'NS', 'B1X' => 'NS', 'B2X' => 'NS',
        'B1Y' => 'NS', 'B2Y' => 'NS', 'B2Z' => 'NS', 'B3Z' => 'NS',
        // C
        'C0A' => 'PE', 'C1A' => 'PE',
        'C0B' => 'PE', 'C1B' => 'PE', 'C1C' => 'PE', 'C1E' => 'PE', 'C1N' => 'PE',
        // E
        'E1A' => 'NB', 'E2A' => 'NB',
        'E3A' => 'NB', 'E4A' => 'NB', 'E5A' => 'NB', 'E6A' => 'NB', 'E7A' => 'NB', 'E8A' => 'NB',
        'E9A' => 'NB', 'E1B' => 'NB', 'E3B' => 'NB', 'E3G' => 'NB', 'E4B' => 'NB', 'E5B' => 'NB', 'E6B' => 'NB',
        'E7B' => 'NB', 'E8B' => 'NB', 'E9B' => 'NB', 'E1C' => 'NB', 'E3C' => 'NB', 'E4C' => 'NB',
        'E5C' => 'NB', 'E6C' => 'NB', 'E7C' => 'NB', 'E8C' => 'NB', 'E9C' => 'NB', 'E1E' => 'NB',
        'E2E' => 'NB', 'E3E' => 'NB', 'E4E' => 'NB', 'E5E' => 'NB', 'E6E' => 'NB', 'E7E' => 'NB',
        'E8E' => 'NB', 'E9E' => 'NB', 'E1G' => 'NB', 'E2G' => 'NB', 'E4G' => 'NB', 'E5G' => 'NB',
        'E6G' => 'NB', 'E7G' => 'NB', 'E8G' => 'NB', 'E9G' => 'NB', 'E1H' => 'NB', 'E2H' => 'NB',
        'E4H' => 'NB', 'E5H' => 'NB', 'E6H' => 'NB', 'E7H' => 'NB', 'E9H' => 'NB', 'E1J' => 'NB',
        'E2J' => 'NB', 'E4J' => 'NB', 'E5J' => 'NB', 'E6J' => 'NB', 'E7J' => 'NB', 'E8J' => 'NB',
        'E2K' => 'NB', 'E4K' => 'NB', 'E5K' => 'NB', 'E6K' => 'NB', 'E7K' => 'NB', 'E8K' => 'NB',
        'E2L' => 'NB', 'E3L' => 'NB', 'E4L' => 'NB', 'E5L' => 'NB', 'E6L' => 'NB', 'E7L' => 'NB',
        'E8L' => 'NB', 'E2M' => 'NB', 'E4M' => 'NB', 'E5M' => 'NB', 'E7M' => 'NB', 'E8M' => 'NB',
        'E1N' => 'NB', 'E2N' => 'NB', 'E3N' => 'NB', 'E4N' => 'NB', 'E5N' => 'NB', 'E7N' => 'NB',
        'E8N' => 'NB', 'E2P' => 'NB', 'E4P' => 'NB', 'E5P' => 'NB', 'E7P' => 'NB', 'E8P' => 'NB',
        'E2R' => 'NB', 'E4R' => 'NB', 'E5R' => 'NB', 'E8R' => 'NB', 'E2S' => 'NB', 'E4S' => 'NB',
        'E5S' => 'NB', 'E8S' => 'NB', 'E4T' => 'NB', 'E5T' => 'NB', 'E8T' => 'NB', 'E1V' => 'NB',
        'E2V' => 'NB', 'E3V' => 'NB', 'E4V' => 'NB', 'E5V' => 'NB', 'E1W' => 'NB', 'E4W' => 'NB',
        'E1X' => 'NB', 'E4X' => 'NB', 'E3Y' => 'NB', 'E4Y' => 'NB', 'E3Z' => 'NB', 'E4Z' => 'NB',
        // G
        'G0A' => 'QC', 'G0B' => 'QC', 'G0C' => 'QC', 'G0E' => 'QC', 'G0G' => 'QC', 'G0H' => 'QC',
        'G0J' => 'QC', 'G0K' => 'QC', 'G0L' => 'QC', 'G0M' => 'QC', 'G0N' => 'QC', 'G0P' => 'QC',
        'G0R' => 'QC', 'G0S' => 'QC', 'G0T' => 'QC', 'G0V' => 'QC', 'G0W' => 'QC', 'G0X' => 'QC',
        'G0Y' => 'QC', 'G0Z' => 'QC', 'G1A' => 'QC', 'G1B' => 'QC', 'G1C' => 'QC', 'G1E' => 'QC',
        'G1G' => 'QC', 'G1H' => 'QC', 'G1J' => 'QC', 'G1K' => 'QC', 'G1L' => 'QC', 'G1M' => 'QC',
        'G1N' => 'QC', 'G1P' => 'QC', 'G1R' => 'QC', 'G1S' => 'QC', 'G1T' => 'QC', 'G1V' => 'QC',
        'G1W' => 'QC', 'G1X' => 'QC', 'G1Y' => 'QC', 'G2A' => 'QC', 'G2B' => 'QC', 'G2C' => 'QC',
        'G2E' => 'QC', 'G2G' => 'QC', 'G2J' => 'QC', 'G2K' => 'QC', 'G2L' => 'QC', 'G2M' => 'QC',
        'G2N' => 'QC', 'G3A' => 'QC', 'G3B' => 'QC', 'G3C' => 'QC', 'G3E' => 'QC', 'G3G' => 'QC',
        'G3H' => 'QC', 'G3J' => 'QC', 'G3K' => 'QC', 'G3L' => 'QC', 'G3M' => 'QC', 'G3N' => 'QC',
        'G3Z' => 'QC', 'G4A' => 'QC', 'G4R' => 'QC', 'G4S' => 'QC', 'G4T' => 'QC', 'G4V' => 'QC',
        'G4W' => 'QC', 'G4X' => 'QC', 'G4Z' => 'QC', 'G5A' => 'QC', 'G5B' => 'QC', 'G5C' => 'QC',
        'G5H' => 'QC', 'G5J' => 'QC', 'G5L' => 'QC', 'G5M' => 'QC', 'G5N' => 'QC', 'G5R' => 'QC',
        'G5T' => 'QC', 'G5V' => 'QC', 'G5X' => 'QC', 'G5Y' => 'QC', 'G5Z' => 'QC', 'G6A' => 'QC',
        'G6B' => 'QC', 'G6C' => 'QC', 'G6E' => 'QC', 'G6G' => 'QC', 'G6H' => 'QC', 'G6J' => 'QC',
        'G6K' => 'QC', 'G6L' => 'QC', 'G6P' => 'QC', 'G6R' => 'QC', 'G6S' => 'QC', 'G6T' => 'QC',
        'G6V' => 'QC', 'G6W' => 'QC', 'G6X' => 'QC', 'G6Z' => 'QC', 'G7A' => 'QC', 'G7B' => 'QC',
        'G7G' => 'QC', 'G7H' => 'QC', 'G7J' => 'QC', 'G7K' => 'QC', 'G7N' => 'QC', 'G7P' => 'QC',
        'G7S' => 'QC', 'G7T' => 'QC', 'G7X' => 'QC', 'G7Y' => 'QC', 'G7Z' => 'QC', 'G8A' => 'QC',
        'G8B' => 'QC', 'G8C' => 'QC', 'G8E' => 'QC', 'G8G' => 'QC', 'G8H' => 'QC', 'G8J' => 'QC',
        'G8K' => 'QC', 'G8L' => 'QC', 'G8M' => 'QC', 'G8N' => 'QC', 'G8P' => 'QC', 'G8T' => 'QC',
        'G8V' => 'QC', 'G8W' => 'QC', 'G8Y' => 'QC', 'G8Z' => 'QC', 'G9A' => 'QC', 'G9B' => 'QC',
        'G9C' => 'QC', 'G9H' => 'QC', 'G9N' => 'QC', 'G9P' => 'QC', 'G9R' => 'QC', 'G9T' => 'QC',
        'G9X' => 'QC',
        // H
        'H0M' => 'QC', 'H1A' => 'QC', 'H1B' => 'QC', 'H1C' => 'QC', 'H1E' => 'QC',
        'H1G' => 'QC', 'H1H' => 'QC', 'H1J' => 'QC', 'H1K' => 'QC', 'H1L' => 'QC', 'H1M' => 'QC',
        'H1N' => 'QC', 'H1P' => 'QC', 'H1R' => 'QC', 'H1S' => 'QC', 'H1T' => 'QC', 'H1V' => 'QC',
        'H1W' => 'QC', 'H1X' => 'QC', 'H1Y' => 'QC', 'H1Z' => 'QC', 'H2A' => 'QC', 'H2B' => 'QC',
        'H2C' => 'QC', 'H2E' => 'QC', 'H2G' => 'QC', 'H2H' => 'QC', 'H2J' => 'QC', 'H2K' => 'QC',
        'H2L' => 'QC', 'H2M' => 'QC', 'H2N' => 'QC', 'H2P' => 'QC', 'H2R' => 'QC', 'H2S' => 'QC',
        'H2T' => 'QC', 'H2V' => 'QC', 'H2W' => 'QC', 'H2X' => 'QC', 'H2Y' => 'QC', 'H2Z' => 'QC',
        'H3A' => 'QC', 'H3B' => 'QC', 'H3C' => 'QC', 'H3E' => 'QC', 'H3G' => 'QC', 'H3H' => 'QC',
        'H3J' => 'QC', 'H3K' => 'QC', 'H3L' => 'QC', 'H3M' => 'QC', 'H3N' => 'QC', 'H3P' => 'QC',
        'H3R' => 'QC', 'H3S' => 'QC', 'H3T' => 'QC', 'H3V' => 'QC', 'H3W' => 'QC', 'H3X' => 'QC',
        'H3Y' => 'QC', 'H3Z' => 'QC', 'H4A' => 'QC', 'H4B' => 'QC', 'H4C' => 'QC', 'H4E' => 'QC',
        'H4G' => 'QC', 'H4H' => 'QC', 'H4J' => 'QC', 'H4K' => 'QC', 'H4L' => 'QC', 'H4M' => 'QC',
        'H4N' => 'QC', 'H4P' => 'QC', 'H4R' => 'QC', 'H4S' => 'QC', 'H4T' => 'QC', 'H4V' => 'QC',
        'H4W' => 'QC', 'H4X' => 'QC', 'H4Y' => 'QC', 'H4Z' => 'QC', 'H5A' => 'QC', 'H5B' => 'QC',
        'H7A' => 'QC', 'H7B' => 'QC', 'H7C' => 'QC', 'H7E' => 'QC', 'H7G' => 'QC', 'H7H' => 'QC',
        'H7J' => 'QC', 'H7K' => 'QC', 'H7L' => 'QC', 'H7M' => 'QC', 'H7N' => 'QC', 'H7P' => 'QC',
        'H7R' => 'QC', 'H7S' => 'QC', 'H7T' => 'QC', 'H7V' => 'QC', 'H7W' => 'QC', 'H7X' => 'QC',
        'H7Y' => 'QC', 'H8N' => 'QC', 'H8P' => 'QC', 'H8R' => 'QC', 'H8S' => 'QC', 'H8T' => 'QC',
        'H8Y' => 'QC', 'H8Z' => 'QC', 'H9A' => 'QC', 'H9B' => 'QC', 'H9C' => 'QC', 'H9E' => 'QC',
        'H9G' => 'QC', 'H9H' => 'QC', 'H9K' => 'QC', 'H9J' => 'QC', 'H9L' => 'QC', 'H9P' => 'QC', 'H9R' => 'QC',
        'H9S' => 'QC', 'H9W' => 'QC', 'H9X' => 'QC',
        // J
        'J0A' => 'QC', 'J0B' => 'QC', 'J0C' => 'QC',
        'J0E' => 'QC', 'J0G' => 'QC', 'J0H' => 'QC', 'J0J' => 'QC', 'J0K' => 'QC', 'J0L' => 'QC',
        'J0M' => 'QC', 'J0N' => 'QC', 'J0P' => 'QC', 'J0R' => 'QC', 'J0S' => 'QC', 'J0T' => 'QC',
        'J0V' => 'QC', 'J0W' => 'QC', 'J0X' => 'QC', 'J0Y' => 'QC', 'J0Z' => 'QC', 'J1A' => 'QC',
        'J1C' => 'QC', 'J1E' => 'QC', 'J1G' => 'QC', 'J1H' => 'QC', 'J1J' => 'QC', 'J1K' => 'QC',
        'J1L' => 'QC', 'J1M' => 'QC', 'J1N' => 'QC', 'J1R' => 'QC', 'J1S' => 'QC', 'J1T' => 'QC',
        'J1X' => 'QC', 'J1Z' => 'QC', 'J2A' => 'QC', 'J2B' => 'QC', 'J2C' => 'QC', 'J2E' => 'QC',
        'J2G' => 'QC', 'J2H' => 'QC', 'J2J' => 'QC', 'J2K' => 'QC', 'J2L' => 'QC', 'J2M' => 'QC',
        'J2N' => 'QC', 'J2R' => 'QC', 'J2S' => 'QC', 'J2T' => 'QC', 'J2W' => 'QC', 'J2X' => 'QC',
        'J2Y' => 'QC', 'J3A' => 'QC', 'J3B' => 'QC', 'J3E' => 'QC', 'J3G' => 'QC', 'J3H' => 'QC',
        'J3L' => 'QC', 'J3M' => 'QC', 'J3N' => 'QC', 'J3P' => 'QC', 'J3R' => 'QC', 'J3T' => 'QC',
        'J3V' => 'QC', 'J3X' => 'QC', 'J3Y' => 'QC', 'J3Z' => 'QC', 'J4B' => 'QC', 'J4G' => 'QC',
        'J4H' => 'QC', 'J4J' => 'QC', 'J4K' => 'QC', 'J4L' => 'QC', 'J4M' => 'QC', 'J4N' => 'QC',
        'J4P' => 'QC', 'J4R' => 'QC', 'J4S' => 'QC', 'J4T' => 'QC', 'J4V' => 'QC', 'J4W' => 'QC',
        'J4X' => 'QC', 'J4Y' => 'QC', 'J4Z' => 'QC', 'J5A' => 'QC', 'J5B' => 'QC', 'J5C' => 'QC',
        'J5J' => 'QC', 'J5K' => 'QC', 'J5L' => 'QC', 'J5M' => 'QC', 'J5R' => 'QC', 'J5T' => 'QC',
        'J5V' => 'QC', 'J5W' => 'QC', 'J5X' => 'QC', 'J5Y' => 'QC', 'J5Z' => 'QC', 'J6A' => 'QC',
        'J6E' => 'QC', 'J6J' => 'QC', 'J6K' => 'QC', 'J6N' => 'QC', 'J6R' => 'QC', 'J6S' => 'QC',
        'J6T' => 'QC', 'J6V' => 'QC', 'J6W' => 'QC', 'J6X' => 'QC', 'J6Y' => 'QC', 'J6Z' => 'QC',
        'J7A' => 'QC', 'J7B' => 'QC', 'J7C' => 'QC', 'J7E' => 'QC', 'J7G' => 'QC', 'J7H' => 'QC',
        'J7J' => 'QC', 'J7K' => 'QC', 'J7L' => 'QC', 'J7M' => 'QC', 'J7N' => 'QC', 'J7P' => 'QC',
        'J7R' => 'QC', 'J7T' => 'QC', 'J7V' => 'QC', 'J7W' => 'QC', 'J7X' => 'QC', 'J7Y' => 'QC',
        'J7Z' => 'QC', 'J8A' => 'QC', 'J8B' => 'QC', 'J8C' => 'QC', 'J8E' => 'QC', 'J8G' => 'QC',
        'J8H' => 'QC', 'J8L' => 'QC', 'J8M' => 'QC', 'J8N' => 'QC', 'J8P' => 'QC', 'J8R' => 'QC',
        'J8T' => 'QC', 'J8V' => 'QC', 'J8X' => 'QC', 'J8Y' => 'QC', 'J8Z' => 'QC', 'J9A' => 'QC',
        'J9B' => 'QC', 'J9E' => 'QC', 'J9H' => 'QC', 'J9J' => 'QC', 'J9L' => 'QC', 'J9P' => 'QC',
        'J9T' => 'QC', 'J9V' => 'QC', 'J9X' => 'QC', 'J9Y' => 'QC', 'J9Z' => 'QC',
        // K
        'K0A' => 'ON',
        'K0B' => 'ON', 'K0C' => 'ON', 'K0E' => 'ON', 'K0G' => 'ON', 'K0H' => 'ON', 'K0J' => 'ON',
        'K0K' => 'ON', 'K0L' => 'ON', 'K0M' => 'ON', 'K1A' => 'ON', 'K1B' => 'ON', 'K1C' => 'ON',
        'K1E' => 'ON', 'K1G' => 'ON', 'K1H' => 'ON', 'K1J' => 'ON', 'K1K' => 'ON', 'K1L' => 'ON',
        'K1M' => 'ON', 'K1N' => 'ON', 'K1P' => 'ON', 'K1R' => 'ON', 'K1S' => 'ON', 'K1T' => 'ON',
        'K1V' => 'ON', 'K1W' => 'ON', 'K1X' => 'ON', 'K1Y' => 'ON', 'K1Z' => 'ON', 'K2A' => 'ON',
        'K2B' => 'ON', 'K2C' => 'ON', 'K2E' => 'ON', 'K2G' => 'ON', 'K2H' => 'ON', 'K2J' => 'ON',
        'K2K' => 'ON', 'K2L' => 'ON', 'K2M' => 'ON', 'K2P' => 'ON', 'K2R' => 'ON', 'K2S' => 'ON',
        'K2T' => 'ON', 'K2V' => 'ON', 'K2W' => 'ON', 'K4A' => 'ON', 'K4B' => 'ON', 'K4C' => 'ON',
        'K4K' => 'ON', 'K4M' => 'ON', 'K4P' => 'ON', 'K4R' => 'ON', 'K6A' => 'ON', 'K6H' => 'ON',
        'K6J' => 'ON', 'K6K' => 'ON', 'K6T' => 'ON', 'K6V' => 'ON', 'K7A' => 'ON', 'K7C' => 'ON',
        'K7G' => 'ON', 'K7H' => 'ON', 'K7K' => 'ON', 'K7L' => 'ON', 'K7M' => 'ON', 'K7N' => 'ON',
        'K7P' => 'ON', 'K7R' => 'ON', 'K7S' => 'ON', 'K7V' => 'ON', 'K8A' => 'ON', 'K8B' => 'ON',
        'K8H' => 'ON', 'K8N' => 'ON', 'K8P' => 'ON', 'K8R' => 'ON', 'K8V' => 'ON', 'K9A' => 'ON',
        'K9H' => 'ON', 'K9J' => 'ON', 'K9K' => 'ON', 'K9L' => 'ON', 'K9V' => 'ON',
        // L
        'L0A' => 'ON',
        'L0B' => 'ON', 'L0C' => 'ON', 'L0E' => 'ON', 'L0G' => 'ON', 'L0H' => 'ON', 'L0J' => 'ON',
        'L0K' => 'ON', 'L0L' => 'ON', 'L0M' => 'ON', 'L0N' => 'ON', 'L0P' => 'ON', 'L0R' => 'ON',
        'L0S' => 'ON', 'L1A' => 'ON', 'L1B' => 'ON', 'L1C' => 'ON', 'L1E' => 'ON',
        'L1G' => 'ON', 'L1H' => 'ON', 'L1J' => 'ON', 'L1K' => 'ON', 'L1L' => 'ON', 'L1M' => 'ON',
        'L1N' => 'ON', 'L1P' => 'ON', 'L1R' => 'ON', 'L1S' => 'ON', 'L1T' => 'ON', 'L1V' => 'ON',
        'L1W' => 'ON', 'L1X' => 'ON', 'L1Y' => 'ON', 'L1Z' => 'ON', 'L2A' => 'ON', 'L2E' => 'ON',
        'L2G' => 'ON', 'L2H' => 'ON', 'L2J' => 'ON', 'L2M' => 'ON', 'L2N' => 'ON', 'L2P' => 'ON',
        'L2R' => 'ON', 'L2S' => 'ON', 'L2T' => 'ON', 'L2V' => 'ON', 'L2W' => 'ON', 'L3B' => 'ON',
        'L3C' => 'ON', 'L3K' => 'ON', 'L3M' => 'ON', 'L3P' => 'ON', 'L3R' => 'ON', 'L3S' => 'ON',
        'L3T' => 'ON', 'L3V' => 'ON', 'L3X' => 'ON', 'L3Y' => 'ON', 'L3Z' => 'ON',
        'L4A' => 'ON', 'L4B' => 'ON', 'L4C' => 'ON', 'L4E' => 'ON', 'L4G' => 'ON', 'L4H' => 'ON',
        'L4J' => 'ON', 'L4K' => 'ON', 'L4L' => 'ON', 'L4M' => 'ON', 'L4N' => 'ON', 'L4P' => 'ON',
        'L4R' => 'ON', 'L4S' => 'ON', 'L4T' => 'ON', 'L4V' => 'ON', 'L4W' => 'ON', 'L4X' => 'ON',
        'L4Y' => 'ON', 'L4Z' => 'ON', 'L5A' => 'ON', 'L5B' => 'ON', 'L5C' => 'ON', 'L5E' => 'ON',
        'L5G' => 'ON', 'L5H' => 'ON', 'L5J' => 'ON', 'L5K' => 'ON', 'L5L' => 'ON', 'L5M' => 'ON',
        'L5N' => 'ON', 'L5P' => 'ON', 'L5R' => 'ON', 'L5S' => 'ON', 'L5T' => 'ON', 'L5V' => 'ON',
        'L5W' => 'ON', 'L6A' => 'ON', 'L6B' => 'ON', 'L6C' => 'ON', 'L6E' => 'ON', 'L6G' => 'ON',
        'L6H' => 'ON', 'L6J' => 'ON', 'L6K' => 'ON', 'L6L' => 'ON', 'L6M' => 'ON', 'L6P' => 'ON',
        'L6R' => 'ON', 'L6S' => 'ON', 'L6T' => 'ON', 'L6V' => 'ON', 'L6W' => 'ON', 'L6X' => 'ON',
        'L6Y' => 'ON', 'L6Z' => 'ON', 'L7A' => 'ON', 'L7B' => 'ON', 'L7C' => 'ON', 'L7E' => 'ON',
        'L7G' => 'ON', 'L7J' => 'ON', 'L7K' => 'ON', 'L7L' => 'ON', 'L7M' => 'ON', 'L7N' => 'ON',
        'L7P' => 'ON', 'L7R' => 'ON', 'L7S' => 'ON', 'L7T' => 'ON', 'L7W' => 'ON', 'L8E' => 'ON',
        'L8G' => 'ON', 'L8H' => 'ON', 'L8J' => 'ON', 'L8K' => 'ON', 'L8L' => 'ON', 'L8M' => 'ON',
        'L8N' => 'ON', 'L8P' => 'ON', 'L8R' => 'ON', 'L8S' => 'ON', 'L8T' => 'ON', 'L8V' => 'ON',
        'L8W' => 'ON', 'L9A' => 'ON', 'L9B' => 'ON', 'L9C' => 'ON', 'L9G' => 'ON', 'L9H' => 'ON',
        'L9K' => 'ON', 'L9L' => 'ON', 'L9M' => 'ON', 'L9N' => 'ON', 'L9P' => 'ON', 'L9R' => 'ON',
        'L9S' => 'ON', 'L9T' => 'ON', 'L9V' => 'ON', 'L9W' => 'ON', 'L9Y' => 'ON', 'L9Z' => 'ON',
        // M
        'M1B' => 'ON', 'M1C' => 'ON', 'M1E' => 'ON', 'M1G' => 'ON', 'M1H' => 'ON', 'M1J' => 'ON',
        'M1K' => 'ON', 'M1L' => 'ON', 'M1M' => 'ON', 'M1N' => 'ON', 'M1P' => 'ON', 'M1R' => 'ON',
        'M1S' => 'ON', 'M1T' => 'ON', 'M1V' => 'ON', 'M1W' => 'ON', 'M1X' => 'ON', 'M2H' => 'ON',
        'M2J' => 'ON', 'M2K' => 'ON', 'M2L' => 'ON', 'M2M' => 'ON', 'M2N' => 'ON', 'M2P' => 'ON',
        'M2R' => 'ON', 'M3A' => 'ON', 'M3B' => 'ON', 'M3C' => 'ON', 'M3H' => 'ON', 'M3J' => 'ON',
        'M3K' => 'ON', 'M3L' => 'ON', 'M3M' => 'ON', 'M3N' => 'ON', 'M4A' => 'ON', 'M4B' => 'ON',
        'M4C' => 'ON', 'M4E' => 'ON', 'M4G' => 'ON', 'M4H' => 'ON', 'M4J' => 'ON', 'M4K' => 'ON',
        'M4L' => 'ON', 'M4M' => 'ON', 'M4N' => 'ON', 'M4P' => 'ON', 'M4R' => 'ON', 'M4S' => 'ON',
        'M4T' => 'ON', 'M4V' => 'ON', 'M4W' => 'ON', 'M4X' => 'ON', 'M4Y' => 'ON', 'M5A' => 'ON',
        'M5B' => 'ON', 'M5C' => 'ON', 'M5E' => 'ON', 'M5G' => 'ON', 'M5H' => 'ON', 'M5J' => 'ON',
        'M5K' => 'ON', 'M5L' => 'ON', 'M5M' => 'ON', 'M5N' => 'ON', 'M5P' => 'ON', 'M5R' => 'ON',
        'M5S' => 'ON', 'M5T' => 'ON', 'M5V' => 'ON', 'M5W' => 'ON', 'M5X' => 'ON', 'M6A' => 'ON',
        'M6B' => 'ON', 'M6C' => 'ON', 'M6E' => 'ON', 'M6G' => 'ON', 'M6H' => 'ON', 'M6J' => 'ON',
        'M6K' => 'ON', 'M6L' => 'ON', 'M6M' => 'ON', 'M6N' => 'ON', 'M6P' => 'ON', 'M6R' => 'ON',
        'M6S' => 'ON', 'M7A' => 'ON', 'M7Y' => 'ON', 'M8V' => 'ON', 'M8W' => 'ON', 'M8X' => 'ON',
        'M8Y' => 'ON', 'M8Z' => 'ON', 'M9A' => 'ON', 'M9B' => 'ON', 'M9C' => 'ON', 'M9L' => 'ON',
        'M9M' => 'ON', 'M9N' => 'ON', 'M9P' => 'ON', 'M9R' => 'ON', 'M9V' => 'ON', 'M9W' => 'ON',
        // N
        'N0A' => 'ON', 'N0B' => 'ON', 'N0C' => 'ON', 'N0E' => 'ON', 'N0G' => 'ON', 'N0H' => 'ON',
        'N0J' => 'ON', 'N0K' => 'ON', 'N0L' => 'ON', 'N0M' => 'ON', 'N0N' => 'ON', 'N0P' => 'ON',
        'N0R' => 'ON', 'N1A' => 'ON', 'N1C' => 'ON', 'N1E' => 'ON', 'N1G' => 'ON', 'N1H' => 'ON',
        'N1K' => 'ON', 'N1L' => 'ON', 'N1M' => 'ON', 'N1P' => 'ON', 'N1R' => 'ON', 'N1S' => 'ON',
        'N1T' => 'ON', 'N2A' => 'ON', 'N2B' => 'ON', 'N2C' => 'ON', 'N2E' => 'ON', 'N2G' => 'ON',
        'N2H' => 'ON', 'N2J' => 'ON', 'N2K' => 'ON', 'N2L' => 'ON', 'N2M' => 'ON', 'N2N' => 'ON',
        'N2P' => 'ON', 'N2R' => 'ON', 'N2T' => 'ON', 'N2V' => 'ON', 'N2Z' => 'ON', 'N3A' => 'ON',
        'N3B' => 'ON', 'N3C' => 'ON', 'N3E' => 'ON', 'N3H' => 'ON', 'N3L' => 'ON', 'N3P' => 'ON',
        'N3R' => 'ON', 'N3S' => 'ON', 'N3T' => 'ON', 'N3V' => 'ON', 'N3W' => 'ON', 'N3Y' => 'ON',
        'N4B' => 'ON', 'N4G' => 'ON', 'N4K' => 'ON', 'N4L' => 'ON', 'N4N' => 'ON', 'N4S' => 'ON',
        'N4T' => 'ON', 'N4V' => 'ON', 'N4W' => 'ON', 'N4X' => 'ON', 'N4Z' => 'ON', 'N5A' => 'ON',
        'N5C' => 'ON', 'N5H' => 'ON', 'N5L' => 'ON', 'N5P' => 'ON', 'N5R' => 'ON', 'N5V' => 'ON',
        'N5W' => 'ON', 'N5X' => 'ON', 'N5Y' => 'ON', 'N5Z' => 'ON',
        'N6A' => 'ON', 'N6B' => 'ON', 'N6C' => 'ON',
        'N6E' => 'ON', 'N6G' => 'ON', 'N6H' => 'ON', 'N6J' => 'ON', 'N6K' => 'ON', 'N6L' => 'ON',
        'N6M' => 'ON', 'N6N' => 'ON', 'N6P' => 'ON',
        'N7A' => 'ON', 'N7G' => 'ON', 'N7L' => 'ON',
        'N7M' => 'ON', 'N7S' => 'ON', 'N7T' => 'ON', 'N7V' => 'ON', 'N7W' => 'ON', 'N7X' => 'ON',
        'N8A' => 'ON', 'N8H' => 'ON', 'N8M' => 'ON', 'N8N' => 'ON', 'N8P' => 'ON', 'N8R' => 'ON',
        'N8S' => 'ON', 'N8T' => 'ON', 'N8V' => 'ON', 'N8W' => 'ON', 'N8X' => 'ON', 'N8Y' => 'ON',
        'N9A' => 'ON', 'N9B' => 'ON', 'N9C' => 'ON', 'N9E' => 'ON', 'N9G' => 'ON', 'N9H' => 'ON',
        'N9J' => 'ON', 'N9K' => 'ON', 'N9V' => 'ON', 'N9Y' => 'ON',
        // P
        'P0A' => 'ON', 'P0B' => 'ON',
        'P0C' => 'ON', 'P0E' => 'ON', 'P0G' => 'ON', 'P0H' => 'ON', 'P0J' => 'ON', 'P0K' => 'ON',
        'P0L' => 'ON', 'P0M' => 'ON', 'P0N' => 'ON', 'P0P' => 'ON', 'P0R' => 'ON', 'P0S' => 'ON',
        'P0T' => 'ON', 'P0V' => 'ON', 'P0W' => 'ON', 'P0X' => 'ON', 'P0Y' => 'ON', 'P1A' => 'ON',
        'P1B' => 'ON', 'P1C' => 'ON', 'P1H' => 'ON', 'P1L' => 'ON', 'P1P' => 'ON', 'P2A' => 'ON',
        'P2B' => 'ON', 'P2N' => 'ON', 'P3A' => 'ON', 'P3B' => 'ON', 'P3C' => 'ON', 'P3E' => 'ON',
        'P3G' => 'ON', 'P3L' => 'ON', 'P3N' => 'ON', 'P3P' => 'ON', 'P3Y' => 'ON', 'P4N' => 'ON',
        'P4P' => 'ON', 'P4R' => 'ON', 'P5A' => 'ON', 'P5E' => 'ON', 'P5N' => 'ON', 'P6A' => 'ON',
        'P6B' => 'ON', 'P6C' => 'ON', 'P7A' => 'ON', 'P7B' => 'ON', 'P7C' => 'ON', 'P7E' => 'ON',
        'P7G' => 'ON', 'P7J' => 'ON', 'P7K' => 'ON', 'P7L' => 'ON', 'P8N' => 'ON', 'P8T' => 'ON',
        'P9A' => 'ON', 'P9N' => 'ON',
        // R
        'R0A' => 'MB', 'R0B' => 'MB', 'R0C' => 'MB', 'R0E' => 'MB',
        'R0G' => 'MB', 'R0H' => 'MB', 'R0J' => 'MB', 'R0K' => 'MB', 'R0L' => 'MB', 'R0M' => 'MB',
        'R1A' => 'MB', 'R1B' => 'MB', 'R1N' => 'MB', 'R2C' => 'MB', 'R2E' => 'MB', 'R2G' => 'MB',
        'R2H' => 'MB', 'R2J' => 'MB', 'R2K' => 'MB', 'R2L' => 'MB', 'R2M' => 'MB', 'R2N' => 'MB',
        'R2P' => 'MB', 'R2R' => 'MB', 'R2V' => 'MB', 'R2W' => 'MB', 'R2X' => 'MB', 'R2Y' => 'MB',
        'R3A' => 'MB', 'R3B' => 'MB', 'R3C' => 'MB', 'R3E' => 'MB', 'R3G' => 'MB', 'R3H' => 'MB',
        'R3J' => 'MB', 'R3K' => 'MB', 'R3L' => 'MB', 'R3M' => 'MB', 'R3N' => 'MB', 'R3P' => 'MB',
        'R3R' => 'MB', 'R3S' => 'MB', 'R3T' => 'MB', 'R3V' => 'MB', 'R3W' => 'MB', 'R3X' => 'MB',
        'R3Y' => 'MB', 'R4A' => 'MB', 'R4H' => 'MB', 'R4J' => 'MB', 'R4K' => 'MB', 'R4L' => 'MB',
        'R5A' => 'MB', 'R5G' => 'MB', 'R5H' => 'MB', 'R6M' => 'MB', 'R6W' => 'MB', 'R7A' => 'MB',
        'R7B' => 'MB', 'R7C' => 'MB', 'R7N' => 'MB', 'R8A' => 'MB', 'R8N' => 'MB', 'R9A' => 'MB',
        // S
        'S0A' => 'SK',
        'S0C' => 'SK', 'S0E' => 'SK', 'S0G' => 'SK', 'S0H' => 'SK', 'S0J' => 'SK', 'S0K' => 'SK',
        'S0L' => 'SK', 'S0M' => 'SK', 'S0N' => 'SK', 'S0P' => 'SK', 'S2V' => 'SK', 'S3N' => 'SK',
        'S4A' => 'SK', 'S4H' => 'SK', 'S4L' => 'SK', 'S4N' => 'SK', 'S4P' => 'SK', 'S4R' => 'SK',
        'S4S' => 'SK', 'S4T' => 'SK', 'S4V' => 'SK', 'S4W' => 'SK', 'S4X' => 'SK', 'S4Y' => 'SK',
        'S4Z' => 'SK', 'S6H' => 'SK', 'S6J' => 'SK', 'S6K' => 'SK', 'S6V' => 'SK', 'S6W' => 'SK',
        'S6X' => 'SK', 'S7H' => 'SK', 'S7J' => 'SK', 'S7K' => 'SK', 'S7L' => 'SK', 'S7M' => 'SK',
        'S7N' => 'SK', 'S7P' => 'SK', 'S7R' => 'SK', 'S7S' => 'SK', 'S7T' => 'SK', 'S7V' => 'SK',
        'S7W' => 'SK', 'S9A' => 'SK', 'S9H' => 'SK', 'S9V' => 'SK', 'S9W' => 'SK', 'S9X' => 'SK',
        // T
        'T0A' => 'AB', 'T0B' => 'AB', 'T0C' => 'AB', 'T0E' => 'AB', 'T0G' => 'AB', 'T0H' => 'AB',
        'T0J' => 'AB', 'T0K' => 'AB', 'T0L' => 'AB', 'T0M' => 'AB', 'T0P' => 'AB', 'T0V' => 'AB',
        'T1A' => 'AB', 'T1B' => 'AB', 'T1C' => 'AB', 'T1G' => 'AB', 'T1H' => 'AB', 'T1J' => 'AB',
        'T1K' => 'AB', 'T1L' => 'AB', 'T1M' => 'AB', 'T1P' => 'AB', 'T1R' => 'AB', 'T1S' => 'AB',
        'T1V' => 'AB', 'T1W' => 'AB', 'T1X' => 'AB', 'T1Y' => 'AB', 'T2A' => 'AB', 'T2B' => 'AB',
        'T2C' => 'AB', 'T2E' => 'AB', 'T2G' => 'AB', 'T2H' => 'AB', 'T2J' => 'AB', 'T2K' => 'AB',
        'T2L' => 'AB', 'T2M' => 'AB', 'T2N' => 'AB', 'T2P' => 'AB', 'T2R' => 'AB', 'T2S' => 'AB',
        'T2T' => 'AB', 'T2V' => 'AB', 'T2W' => 'AB', 'T2X' => 'AB', 'T2Y' => 'AB', 'T2Z' => 'AB',
        'T3A' => 'AB', 'T3B' => 'AB', 'T3C' => 'AB', 'T3E' => 'AB', 'T3G' => 'AB', 'T3H' => 'AB',
        'T3J' => 'AB', 'T3K' => 'AB', 'T3L' => 'AB', 'T3M' => 'AB', 'T3N' => 'AB', 'T3P' => 'AB',
        'T3R' => 'AB', 'T3S' => 'AB', 'T3Z' => 'AB', 'T4A' => 'AB', 'T4B' => 'AB', 'T4C' => 'AB',
        'T4E' => 'AB', 'T4G' => 'AB', 'T4H' => 'AB', 'T4J' => 'AB', 'T4L' => 'AB', 'T4N' => 'AB',
        'T4P' => 'AB', 'T4R' => 'AB', 'T4S' => 'AB', 'T4T' => 'AB', 'T4V' => 'AB', 'T4X' => 'AB',
        'T5A' => 'AB', 'T5B' => 'AB', 'T5C' => 'AB', 'T5E' => 'AB', 'T5G' => 'AB', 'T5H' => 'AB',
        'T5J' => 'AB', 'T5K' => 'AB', 'T5L' => 'AB', 'T5M' => 'AB', 'T5N' => 'AB', 'T5P' => 'AB',
        'T5R' => 'AB', 'T5S' => 'AB', 'T5T' => 'AB', 'T5V' => 'AB', 'T5W' => 'AB', 'T5X' => 'AB',
        'T5Y' => 'AB', 'T5Z' => 'AB', 'T6A' => 'AB', 'T6B' => 'AB', 'T6C' => 'AB', 'T6E' => 'AB',
        'T6G' => 'AB', 'T6H' => 'AB', 'T6J' => 'AB', 'T6K' => 'AB', 'T6L' => 'AB', 'T6M' => 'AB',
        'T6N' => 'AB', 'T6P' => 'AB', 'T6R' => 'AB', 'T6S' => 'AB', 'T6T' => 'AB', 'T6V' => 'AB',
        'T6W' => 'AB', 'T6X' => 'AB', 'T7A' => 'AB', 'T7E' => 'AB', 'T7N' => 'AB', 'T7P' => 'AB',
        'T7S' => 'AB', 'T7V' => 'AB', 'T7X' => 'AB', 'T7Y' => 'AB', 'T7Z' => 'AB', 'T8A' => 'AB',
        'T8B' => 'AB', 'T8C' => 'AB', 'T8E' => 'AB', 'T8G' => 'AB', 'T8H' => 'AB', 'T8L' => 'AB',
        'T8N' => 'AB', 'T8R' => 'AB', 'T8S' => 'AB', 'T8V' => 'AB', 'T8W' => 'AB', 'T8X' => 'AB',
        'T9A' => 'AB', 'T9C' => 'AB', 'T9E' => 'AB', 'T9G' => 'AB', 'T9H' => 'AB', 'T9J' => 'AB',
        'T9K' => 'AB', 'T9M' => 'AB', 'T9N' => 'AB', 'T9S' => 'AB', 'T9V' => 'AB', 'T9W' => 'AB',
        'T9X' => 'AB',
        // V
        'V0A' => 'BC', 'V0B' => 'BC', 'V0C' => 'BC', 'V0E' => 'BC', 'V0G' => 'BC',
        'V0H' => 'BC', 'V0J' => 'BC', 'V0K' => 'BC', 'V0L' => 'BC', 'V0M' => 'BC', 'V0N' => 'BC',
        'V0P' => 'BC', 'V0R' => 'BC', 'V0S' => 'BC', 'V0T' => 'BC', 'V0V' => 'BC', 'V0W' => 'BC',
        'V0X' => 'BC', 'V1A' => 'BC', 'V1B' => 'BC', 'V1C' => 'BC', 'V1E' => 'BC', 'V1G' => 'BC',
        'V1H' => 'BC', 'V1J' => 'BC', 'V1K' => 'BC', 'V1L' => 'BC', 'V1M' => 'BC', 'V1N' => 'BC',
        'V1P' => 'BC', 'V1R' => 'BC', 'V1S' => 'BC', 'V1T' => 'BC', 'V1V' => 'BC', 'V1W' => 'BC',
        'V1X' => 'BC', 'V1Y' => 'BC', 'V1Z' => 'BC', 'V2A' => 'BC', 'V2B' => 'BC', 'V2C' => 'BC',
        'V2E' => 'BC', 'V2G' => 'BC', 'V2H' => 'BC', 'V2J' => 'BC', 'V2K' => 'BC', 'V2L' => 'BC',
        'V2M' => 'BC', 'V2N' => 'BC', 'V2P' => 'BC', 'V2R' => 'BC', 'V2S' => 'BC', 'V2T' => 'BC',
        'V2V' => 'BC', 'V2W' => 'BC', 'V2X' => 'BC', 'V2Y' => 'BC', 'V2Z' => 'BC', 'V3A' => 'BC',
        'V3B' => 'BC', 'V3C' => 'BC', 'V3E' => 'BC', 'V3G' => 'BC', 'V3H' => 'BC', 'V3J' => 'BC',
        'V3K' => 'BC', 'V3L' => 'BC', 'V3M' => 'BC', 'V3N' => 'BC', 'V3R' => 'BC', 'V3S' => 'BC',
        'V3T' => 'BC', 'V3V' => 'BC', 'V3W' => 'BC', 'V3X' => 'BC', 'V3Y' => 'BC', 'V4A' => 'BC',
        'V4B' => 'BC', 'V4C' => 'BC', 'V4E' => 'BC', 'V4G' => 'BC', 'V4K' => 'BC', 'V4L' => 'BC',
        'V4M' => 'BC', 'V4N' => 'BC', 'V4P' => 'BC', 'V4R' => 'BC', 'V4S' => 'BC', 'V4T' => 'BC',
        'V4V' => 'BC', 'V4W' => 'BC', 'V4X' => 'BC', 'V4Z' => 'BC', 'V5A' => 'BC', 'V5B' => 'BC',
        'V5C' => 'BC', 'V5E' => 'BC', 'V5G' => 'BC', 'V5H' => 'BC', 'V5J' => 'BC', 'V5K' => 'BC',
        'V5L' => 'BC', 'V5M' => 'BC', 'V5N' => 'BC', 'V5P' => 'BC', 'V5R' => 'BC', 'V5S' => 'BC',
        'V5T' => 'BC', 'V5V' => 'BC', 'V5W' => 'BC', 'V5X' => 'BC', 'V5Y' => 'BC', 'V5Z' => 'BC',
        'V6A' => 'BC', 'V6B' => 'BC', 'V6C' => 'BC', 'V6E' => 'BC', 'V6G' => 'BC', 'V6H' => 'BC',
        'V6J' => 'BC', 'V6K' => 'BC', 'V6L' => 'BC', 'V6M' => 'BC', 'V6N' => 'BC', 'V6P' => 'BC',
        'V6R' => 'BC', 'V6S' => 'BC', 'V6T' => 'BC', 'V6V' => 'BC', 'V6W' => 'BC', 'V6X' => 'BC',
        'V6Y' => 'BC', 'V6Z' => 'BC', 'V7A' => 'BC', 'V7B' => 'BC', 'V7C' => 'BC', 'V7E' => 'BC',
        'V7G' => 'BC', 'V7H' => 'BC', 'V7J' => 'BC', 'V7K' => 'BC', 'V7L' => 'BC', 'V7M' => 'BC',
        'V7N' => 'BC', 'V7P' => 'BC', 'V7R' => 'BC', 'V7S' => 'BC', 'V7T' => 'BC', 'V7V' => 'BC',
        'V7W' => 'BC', 'V7X' => 'BC', 'V7Y' => 'BC', 'V8A' => 'BC', 'V8B' => 'BC', 'V8C' => 'BC',
        'V8G' => 'BC', 'V8J' => 'BC', 'V8K' => 'BC', 'V8L' => 'BC', 'V8M' => 'BC', 'V8N' => 'BC',
        'V8P' => 'BC', 'V8R' => 'BC', 'V8S' => 'BC', 'V8T' => 'BC', 'V8V' => 'BC', 'V8W' => 'BC',
        'V8X' => 'BC', 'V8Y' => 'BC', 'V8Z' => 'BC', 'V9A' => 'BC', 'V9B' => 'BC', 'V9C' => 'BC',
        'V9E' => 'BC', 'V9G' => 'BC', 'V9H' => 'BC', 'V9J' => 'BC', 'V9K' => 'BC', 'V9L' => 'BC',
        'V9M' => 'BC', 'V9N' => 'BC', 'V9P' => 'BC', 'V9R' => 'BC', 'V9S' => 'BC', 'V9T' => 'BC',
        'V9V' => 'BC', 'V9W' => 'BC', 'V9X' => 'BC', 'V9Y' => 'BC', 'V9Z' => 'BC',
        // X
        'X0A' => 'NU',
        'X1A' => 'NT', 'X0B' => 'NU', 'X0C' => 'NU', 'X0E' => 'NT', 'X0G' => 'NT',
        // Y
        'Y0A' => 'YT', 'Y1A' => 'YT', 'Y0B' => 'YT'
    );

    /**
     * Attempts to validate a zip/postal code sent to it
     *
     * @param string $zipcode The zip code being provided
     * @return boolean|string string=formatted zipcode,False=not matched,null=unknown
     */
    public function validateZipcode($zipcode)
    {
        $noSpaces = strtoupper(preg_replace('/[ \,\.\-]/', '', $zipcode));
        $matches = array();
        /**
         * Regular expression built up from http://en.wikipedia.org/wiki/Postal_codes_in_Canada#Number_of_possible_postal_codes
         */
        if (preg_match('/^([ABCEGHJKLMNPRSTVXY][0-9][ABCEGHJKLMNPRSTVWXYZ])([0-9][ABCEGHJKLMNPRSTVWXYZ][0-9])$/', $noSpaces, $matches) === 1) {
            $return = $matches[1] . ' ' . $matches[2];
        } else {
            $return = FALSE;
        }
        return $return;
    }

    /**
     * Gets the ISO 3166-2 state subdivision for this country from the zip/postal code
     *
     * May return:
     *  NULL : Means invalid zip/postal code
     *  xx: Unable to match subdivision of country xx
     *  xx-YYY: The country XX and subdivision YYY
     *
     * @throws \Exception If type is not recognised
     * @param string $zip The zipcode/postal code
     * @return string|null NULL=Invalid zipcode for area,string=as much of the subdivision as possible (including the country code)
     */
    public function getStateSubdivisionFromZipcode($zip)
    {
        $zip = $this->validateZipcode($zip);
        if (is_string($zip) === FALSE) {
            return NULL;
        }
        $prefix = mb_substr($zip, 0, 3);
        if (isset($this->stateszips[$prefix]) === TRUE) {
            return 'CA-' . $this->stateszips[$prefix];
        } else {
            return 'CA';
        }
    }

}
