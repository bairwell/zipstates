<?php
/**
 * The Great Britian (UK) postcode class.
 *
 * This work is licensed under the Creative Commons Attribution 3.0 Unported License.
 * To view a copy of this license, visit
 * http://creativecommons.org/licenses/by/3.0/or send
 * a letter to
 * Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 *
 * The postal/zip codes look up in this file are based off the following Wikipedia article
 * http://en.wikipedia.org/wiki/UK_Postcodes
 * http://en.wikipedia.org/wiki/List_of_postcode_areas_in_the_United_Kingdom
 * and is covered under the Creative Commons Attribution 3.0 Unported License .
 *
 * @package Bairwell
 * @subpackage ZipStates
 * @author Richard Chiswell <richard@Bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license Creative Commons Attribution 3.0 Unported License
 */
namespace Bairwell\ZipStates\Countries;

/**
 * Great Britain/United Kingdom
 */
class GB extends Base
{

    /**
     * The UK postal code prefixes to localities.
     *
     * Data from http://en.wikipedia.org/wiki/UK_Postcodes and
     * http://en.wikipedia.org/wiki/List_of_postcode_areas_in_the_United_Kingdom
     * @var array The postcode prefixes to counties
     */
    private $statezips = array(
        'AB' => 'GB-ABD',
        'AL' => 'GB-BAS',
        'B' => 'GB-BIR',
        'BA' => 'GB-BAS',
        'BB' => 'GB-BBD',
        'BD' => 'GB-BRD',
        'BH' => 'GB-BMH',
        'BL' => 'GB-BOL',
        'BN' => 'GB-BNH',
        'BR' => 'GB-BRY',
        'BS' => 'GB-BST',
        'BT' => 'GB-BFS',
        'CA' => 'GB-CMA',
        'CB' => 'GB-CAM',
        'CF' => 'GB-CRF',
        'CH' => 'GB-CHS',
        'CM' => 'GB-ESS',
        'CO' => 'GB-ESS',
        'CR' => 'GB-CRY',
        'CT' => 'GB-KEN',
        'CV' => 'GB-COV',
        'CW' => 'GB-CHS',
        'DA' => 'GB-KEN',
        'DD' => 'GB-DND',
        'DE' => 'GB-DER',
        'DG' => 'GB-DGY',
        'DH' => 'GB-DUR',
        'DL' => 'GB-DAL',
        'DN' => 'GB-DNC',
        'DT' => 'GB-DOR',
        'DY' => 'GB-DUD',
        'E' => 'GB-LND',
        'EC' => 'GB-LND',
        'EH' => 'GB-EDH',
        'EN' => 'GB-ENF',
        'EX' => 'GB-DEV',
        'FK' => 'GB-FAL',
        'FY' => 'GB-BPL',
        'G' => 'GB-GLG',
        'GL' => 'GB-GLS',
        'GU' => 'GB-SRY',
        'HA' => 'GB-HRW',
        'HD' => 'GB-KIR',
        'HG' => 'GB-NYK',
        'HP' => 'GB-HRT',
        'HR' => 'GB-HEF',
        'HS' => 'GB-ELS',
        'HU' => 'GB-KHL',
        'HX' => 'GB-CLD',
        'IG' => 'GB-RDB',
        'IP' => 'GB-SFK',
        'IV' => 'GB-HLD',
        'KA' => 'GB-EAY',
        'KT' => 'GB-KTT',
        'KW' => 'GB-ORK',
        'KY' => 'GB-FIF',
        'L' => 'GB-LIV',
        'LA' => 'GB-LAN',
        'LD' => 'GB-POW',
        'LE' => 'GB-LCE',
        'LL' => 'GB-GWN',
        'LN' => 'GB-LIN',
        'LS' => 'GB-LDS',
        'LU' => 'GB-LUT',
        'M' => 'GB-MAN',
        'ME' => 'GB-MDW',
        'MK' => 'GB-MIK',
        'ML' => 'GB-NLK',
        'N' => 'GB-LND',
        'NE' => 'GB-NET',
        'NG' => 'GB-NGM',
        'NN' => 'GB-NTH',
        'NP' => 'GB-NWP',
        'NR' => 'GB-NFK',
        'NW' => 'GB-LND',
        'OL' => 'GB-OLD',
        'OX' => 'GB-OXF',
        'PA' => 'GB-RFW',
        'PE' => 'GB-PTE',
        'PH' => 'GB-PKN',
        'PL' => 'GB-PLY',
        'PO' => 'GB-POR',
        'PR' => 'GB-LAN',
        'RG' => 'GB-RDG',
        'RH' => 'GB-SRY',
        'RM' => 'GB-HAV',
        'S' => 'GB-SHF',
        'SA' => 'GB-SWA',
        'SE' => 'GB-LND',
        'SG' => 'GB-HRT',
        'SK' => 'GB-SKP',
        'SL' => 'GB-SLG',
        'SM' => 'GB-STN',
        'SN' => 'GB-SWD',
        'SO' => 'GB-STH',
        'SP' => 'GB-WIL',
        'SR' => 'GB-SND',
        'SS' => 'GB-SOS',
        'ST' => 'GB-STE',
        'SW' => 'GB-LND',
        'SY' => 'GB-SHR',
        'TA' => 'GB-SOM',
        'TD' => 'GB-SCB',
        'TF' => 'GB-TFW',
        'TN' => 'GB-KEN',
        'TQ' => 'GB-TOB',
        'TR' => 'GB-CON',
        'TS' => 'GB-RCC',
        'TW' => 'GB-RIC',
        'UB' => 'GB-EAL',
        'W' => 'GB-LND',
        'WA' => 'GB-WRT',
        'WC' => 'GB-LND',
        'WD' => 'GB-HRT',
        'WF' => 'GB-WKF',
        'WN' => 'GB-WGN',
        'WR' => 'GB-WOR',
        'WS' => 'GB-WLL',
        'WV' => 'GB-WLV',
        'YO' => 'GB-YOR',
        'ZE' => 'GB-ZET',
        'GY' => 'GG', // Guernsey
        'IM' => 'IM', // Isle of Man
        'JE' => 'JE', // Jersey
        'BX' => 'GB' //// Special GB wide postcode for banks and similar
    );

    /**
     * Northern Ireland zip/postal codes
     * All of these start with BT...
     *
     * @var array The postcode prefixes to counties
     */
    private $northernirelandZips = array(
        '18' => 'GB-NDN',
        '19' => 'GB-NDN',
        '20' => 'GB-NDN',
        '21' => 'GB-ARD',
        '22' => 'GB-ARD',
        '23' => 'GB-ARD',
        '24' => 'GB-DOW',
        '25' => 'GB-BNB',
        '26' => 'GB-LSB',
        '27' => 'GB-LSB',
        '28' => 'GB-LSB',
        '29' => 'GB-BFS',
        '30' => 'GB-DOW',
        '31' => 'GB-DOW',
        '32' => 'GB-BNB',
        '33' => 'GB-DOW',
        '34' => 'GB-NYM',
        '35' => 'GB-NYM',
        '36' => 'GB-NTA',
        '37' => 'GB-NTA',
        '38' => 'GB-CKF',
        '39' => 'GB-NTA',
        '40' => 'GB-LRN',
        '41' => 'GB-ANT',
        '42' => 'GB-BLA',
        '43' => 'GB-BLA',
        '44' => 'GB-BLA',
        '45' => 'GB-MFT',
        '46' => 'GB-MFT',
        '47' => 'GB-DRY',
        '48' => 'GB-DRY',
        '49' => 'GB-LMV',
        '50' => 'GB-BFS',
        '51' => 'GB-CLR',
        '52' => 'GB-CLR',
        '53' => 'GB-BLY',
        '54' => 'GB-MYL',
        '55' => 'GB-CLR',
        '56' => 'GB-CLR',
        '57' => 'GB-MYL',
        '58' => 'GB-BFS',
        '59' => 'GB-BFS',
        '60' => 'GB-ARM',
        '61' => 'GB-ARM',
        '62' => 'GB-CGV',
        '63' => 'GB-CGV',
        '64' => 'GB-CGV',
        '65' => 'GB-CGV',
        '66' => 'GB-CGV',
        '67' => 'GB-CGV',
        '68' => 'GB-DGN',
        '69' => 'GB-DGN',
        '70' => 'GB-DGN',
        '71' => 'GB-DGN',
        '72' => 'GB-BFS',
        '73' => 'GB-BFS',
        '74' => 'GB-FER',
        '75' => 'GB-FER',
        '76' => 'GB-DGN',
        '77' => 'GB-DGN',
        '78' => 'GB-OMH',
        '79' => 'GB-OMH',
        '80' => 'GB-CKT',
        '81' => 'GB-STB',
        '82' => 'GB-STB',
        '83' => 'GB-BFS',
        '84' => 'GB-BFS',
        '85' => 'GB-BFS',
        '86' => 'GB-BFS',
        '87' => 'GB-BFS',
        '88' => 'GB-BFS',
        '89' => 'GB-BFS',
        '90' => 'GB-BFS',
        '91' => 'GB-BFS',
        '92' => 'GB-FER',
        '93' => 'GB-FER',
        '94' => 'GB-FER'
    );

    /**
     * Attempts to validate a zip/postal code sent to it
     *
     * @param string $zipcode The zip code being provided
     * @return boolean|string string=formatted zipcode,False=not matched,null=unknown
     */
    public function validateZipcode($zipcode)
    {
        $zipcode = strtoupper(preg_replace('/[ \,\.\-]/', '', $zipcode));
        // Permitted letters depend upon their position in the postcode.
        $alpha1 = '[ABCDEFGHIJKLMNOPRSTUWYZ]'; // Character 1
        $alpha2 = '[ABCDEFGHKLMNOPQRSTUVWXY]'; // Character 2
        $alpha3 = '[ABCDEFGHJKSTUW]'; // Character 3
        $alpha4 = '[ABEHMNPRVWXY]'; // Character 4
        $alpha5 = '[ABDEFGHJLNPQRSTUWXYZ]'; // Character 5
        // Expression for postcodes: AN NAA, ANN NAA, AAN NAA, and AANN NAA with a space
        $pcexp[0] = '/^(' . $alpha1 . '{1}' . $alpha2 . '{0,1}[0-9]{1,2})([0-9]{1}' . $alpha5 . '{2})$/';
        // Expression for postcodes: ANA NAA
        $pcexp[1] = '/^(' . $alpha1 . '{1}[0-9]{1}' . $alpha3 . '{1})([0-9]{1}' . $alpha5 . '{2})$/';
        // Expression for postcodes: AANA NAA
        $pcexp[2] = '/^(' . $alpha1 . '{1}' . $alpha2 . '[0-9]{1}' . $alpha4 . ')([0-9]{1}' . $alpha5 . '{2})$/';
        // Exception for the special postcode GIR 0AA
        $pcexp[3] = '/^(GIR)(0AA)$/';
        // Standard BFPO numbers
        $pcexp[4] = '/^(BFPO)([0-9]{1,4})$/';
        // c/o BFPO numbers
        $pcexp[5] = '/^(BFPO)(C\/O[0-9]{1,3})$/';
        // Overseas Territories
        $pcexp[6] = '/^([A-Z]{4})(1ZZ)$/';
        // Santa
        $pcexp[7] = '/^(SAN)(TA1)$/';
        $pcexp[8] = '/^(AI)(2640)$/';
        // Load up the string to check, converting into lowercase
        // Assume we are not going to find a valid postcode
        $return = FALSE;
        // Check the string against the six types of postcodes
        foreach ($pcexp as $regexp) {
            $matches = array();
            if (preg_match($regexp, $zipcode, $matches) === 1) {
                // Load new postcode back into the form element
                $zipcode = $matches[1] . ' ' . $matches[2];
                // Take account of the special BFPO c/o format
                $zipcode = preg_replace('/C\/O/', 'c/o ', $zipcode);
                // Remember that we have found that the code is valid and break from loop
                if ($zipcode === 'AI 2640') {
                    $zipcode = 'AI-2640';
                }
                $return = $zipcode;
                break;
            }
        }
        /**
         * Old invalid postcodes.
         * D[n] used to be Dublin, but is now unallocated
         */
        if (preg_match('/^(D[0-9])/', $return) === 1) {
            $return = FALSE;
        }
        return $return;
    }

    /**
     * Gets the ISO 3166-2 country subdivision for this country from the state
     *
     * Only really relevant for the UK
     *
     * May return:
     *  NULL : Means invalid zip/postal code
     *  xx: Unable to match subdivision of country xx
     *  xx-YYY: The country XX and subdivision YYY
     *
     * @throws \Exception If type is not recognised
     * @param string $state The state in the format GB-DOR
     * @return string|null NULL=Invalid zipcode for area,string=as much of the subdivision as possible (including the country code)
     */
    public function getCountrySubdivisionFromState($state)
    {
        if (mb_strlen($state) !== 6) {
            throw new \Exception('Invalid state length');
        }
        if (mb_substr($state, 0, 3) !== 'GB-') {
            throw new \Exception('Can only deal with GB states');
        }
        $state = mb_substr($state, 3);
        $statesInEngland = array(
            // Two tier county
            'BKM', 'CAM', 'CMA', 'DBY', 'DEV', 'DOR', 'ESX', 'ESS', 'GLS', 'HAM', 'HRT', 'KEN', 'LAN',
            'LEC', 'LIN', 'NFK', 'NYK', 'NTH', 'NTT', 'OXF', 'SOM', 'STS', 'SFK', 'SRY', 'WAR', 'WOR',
            // London Boroughs
            'BDG', 'BNE', 'BEX', 'BEN', 'BRY', 'CMD', 'CRY', 'EAL', 'ENF', 'GRE', 'HCK', 'HMF',
            'HRY', 'HRW', 'HAV', 'HIL', 'HNS', 'ISL', 'KEC', 'KTT', 'LBH', 'LEW', 'MRT', 'NWM', 'RDB', 'RIC',
            'SWK', 'STN', 'TWH', 'WFT', 'WND', 'WSM',
            // Metropolitan district
            'BNS', 'BIR', 'BOL', 'BRD', 'BUR', 'CLD', 'COV', 'DNC', 'DUD', 'GAT', 'KIR', 'KWL', 'LDS', 'LIV',
            'MAN', 'NET', 'NTY', 'OLD', 'RCH', 'ROT', 'SHN', 'SLF', 'SAW', 'SFT', 'SHF',
            'SOL', 'STY', 'SKP', 'SND', 'TAM', 'TRF', 'WKF', 'WLL', 'WGN', 'WRL', 'WLV',
            // Unitary Authorities
            'BAS', 'BBD', 'BDF', 'BPL', 'BMH', 'BRC', 'BNH', 'BST', 'CBF', 'CHE', 'CHW', 'CON', 'DAL',
            'DER', 'DUR', 'ERY', 'HAL', 'HPL', 'HEF', 'IOW', 'KHL', 'LCE', 'LUT', 'MDW', 'MDB', 'MIK',
            'NEL', 'NLN', 'NSM', 'NBL', 'NGM', 'PTE', 'PLY', 'POL', 'POR', 'RDG', 'RCC', 'RUT', 'SHR',
            'SLG', 'SGC', 'STH', 'SOS', 'STT', 'STE', 'SWD', 'TFW', 'THR', 'TOB', 'WRT', 'SBK', 'WIL',
            'WNM', 'WOK', 'YOR',
            // City corporation
            'LND',
            // Depreciated ones
            'BDF', 'CHS', 'IOS'
        );
        if (in_array($state, $statesInEngland) === TRUE) {
            return 'GB-ENG';
        }
        $statesInNorthernIreland = array(
            'ANT', 'ARD', 'ARM', 'BLA', 'BLY', 'BNB', 'BFS', 'CKF', 'CSR', 'CLR', 'CKT', 'CGV', 'DRY', 'DOW', 'DGN', 'FER',
            'LRN', 'LMV', 'LSB', 'MFT', 'MYL', 'NYM', 'NTA', 'NDN', 'OMH', 'STB'
        );
        if (in_array($state, $statesInNorthernIreland) === TRUE) {
            return 'GB-NIR';
        }
        $statesInScotland = array(
            'ABE', 'ABD', 'ANS', 'AGB', 'CLK', 'DGY', 'DND', 'EAY', 'EDU', 'ELN', 'ERW', 'EDH', 'ELS', 'FAL', 'FIF',
            'GLG', 'HLD', 'IVC', 'MLN', 'MRY', 'NAY', 'NLK', 'ORK', 'PKN', 'RFW', 'SCB', 'ZET', 'SAY', 'SLK', 'STG',
            'WDU'
        );
        if (in_array($state, $statesInScotland) === TRUE) {
            return 'GB-SCT';
        }
        $statesInWales = array(
            'BGW', 'BGE', 'CAY', 'CRF', 'CMN', 'CGN', 'CWY', 'DEN', 'FLN', 'GWN', 'AGY', 'MTY', 'MON', 'NTL', 'NWP', 'PEM',
            'POW', 'RCT', 'SWA', 'TOF', 'VGL', 'WRX'
        );
        if (in_array($state, $statesInWales) === TRUE) {
            return 'GB-WLS';
        }
        throw new \Exception('Unrecognised subdivision of ' . $state);
    }

    /**
     * Gets the ISO 3166-2 country subdivision for this country from the zip/postal code
     *
     * Only really relevant for the UK
     *
     * May return:
     *  NULL : Means invalid zip/postal code
     *  xx: Unable to match subdivision of country xx
     *  xx-YYY: The country XX and subdivision YYY
     *
     * @throws \Exception If type is not recognised
     * @param string $zip The zipcode/postal code
     * @return string|null NULL=Invalid zipcode for area,string=as much of the subdivision as possible (including the country code)
     */
    public function getCountrySubdivisionFromZipcode($zip)
    {
        $state = $this->getStateSubdivisionFromZipcode($zip);
        if ($state === NULL) {
            return NULL;
        }
        if (mb_strlen($state) === 2) {
            return $state;
        }
        return $this->getCountrySubdivisionFromState($state);
    }

    /**
     * Recognise the Northern Ireland postcode codes
     * @param string $zip The zipcode/postal code
     * @return null|string NULL if unmatched,tring=as much of the subdivision as possible (including the country code)
     */
    private function getStateSubdivisionFromZipcodeNorthernIreland($zip)
    {
        $zip = preg_replace('/[ \,\.\-]/', '', $zip);
        preg_match('/^(BT[0-9]{1,2})([0-9]{1}[ABDEFGHJLNPQRSTUWXYZ]{2})$/', $zip, $matches);
        $prefix = mb_substr($matches[1], 2, 2);
        if (isset($this->northernirelandZips[$prefix]) === TRUE) {
            return $this->northernirelandZips[$prefix];
        } else {
            /**
             * Belfast covers BT1-17,BT29,BT58 and BT99
             */
            return 'GB-BFS';
        }
    }

    /**
     * Gets the ISO 3166-2 state subdivision for this country from the zip/postal code
     *
     * May return:
     *  NULL : Means invalid zip/postal code
     *  xx: Unable to match subdivision of country xx
     *  xx-YYY: The country XX and subdivision YYY
     *
     * @throws \Exception If type is not recognised
     * @param string $zip The zipcode/postal code
     * @return string|null NULL=Invalid zipcode for area,string=as much of the subdivision as possible (including the country code)
     */
    public function getStateSubdivisionFromZipcode($zip)
    {
        $zip = $this->validateZipcode($zip);
        if (is_string($zip) === FALSE) {
            return NULL;
        }
        $matches = array();
        $fullMatches = array(
            'AI-2640' => 'AI', // Anguilla
            'SAN TA1' => 'GB', // Santa Special
            'GIR 0AA' => 'GB', // Girobank
            'ASCN 1ZZ' => 'AC', // Ascension Island
            'BBND 1ZZ' => 'IO', // British Indian Ocean Territory
            'BIQQ 1ZZ' => 'AQ', // British Antarctic Territory
            'FIQQ 1ZZ' => 'FK', // Falkland Islands
            'PCRN 1ZZ' => 'PN', // Pitcairn Islands
            'SIQQ 1ZZ' => 'GS', // South Georgia and the South Sandwich Islands
            'STHL 1ZZ' => 'SH', // Saint Helena
            'TDCU 1ZZ' => 'SH', // Tristan da Cunha (classed as part of Saint Helena)
            'TKCA 1ZZ' => 'TC', // Turns and Caicos Islands
            'GX11 1AA' => 'GI' // Gibraltar
        );
        if (isset($fullMatches[$zip]) === TRUE) {
            return $fullMatches[$zip];
        }
        if (preg_match('/^([A-Z][A-Z]?)[0-9]/', $zip, $matches) === 1) {
            $prefix = $matches[1];
            if ($prefix === 'BT') {
                return $this->getStateSubdivisionFromZipcodeNorthernIreland($zip);
            } else if (isset($this->statezips[$prefix]) === TRUE) {
                return $this->statezips[$prefix];
            }
        }
        return NULL;
    }

}
