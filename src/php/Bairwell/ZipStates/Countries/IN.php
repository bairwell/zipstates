<?php
/**
 * The Indian zipstates class.
 *
 * This work is licensed under the Creative Commons Attribution 3.0 Unported License.
 * To view a copy of this license, visit
 * http://creativecommons.org/licenses/by/3.0/or send
 * a letter to
 * Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 *
 * The postal/zip codes look up in this file are based off the following Wikipedia article
 * http://en.wikipedia.org/wiki/Postal_Index_Number
 * and is covered under the Creative Commons Attribution 3.0 Unported License .
 *
 * @package Bairwell
 * @subpackage ZipStates
 * @author Richard Chiswell <richard@Bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license Creative Commons Attribution 3.0 Unported License
 */
namespace Bairwell\ZipStates\Countries;

/**
 * India
 */
class IN extends Base
{

    /**
     * Attempts to validate a zip/postal code sent to it
     *
     * @param string $zipcode The zip code being provided
     * @return boolean|string string=formatted zipcode,False=not matched,null=unknown
     */
    public function validateZipcode($zipcode)
    {
        $noSpaces = strtoupper(preg_replace('/[ \,\.\-]/', '', $zipcode));
        if (preg_match('/^([1-9][0-9]{5})$/', $noSpaces) === 1) {
            $return = $noSpaces;
        } else {
            $return = FALSE;
        }
        return $return;
    }

    /**
     * Gets the ISO 3166-2 state subdivision for this country from the zip/postal code
     *
     * May return:
     *  NULL : Means invalid zip/postal code
     *  xx: Unable to match subdivision of country xx
     *  xx-YYY: The country XX and subdivision YYY
     *
     * @throws \Exception If type is not recognised
     * @param string $zip The zipcode/postal code
     * @return string|null NULL=Invalid zipcode for area,string=as much of the subdivision as possible (including the country code)
     */
    public function getStateSubdivisionFromZipcode($zip)
    {
        $zip = $this->validateZipcode($zip);
        if (is_string($zip) === FALSE) {
            return NULL;
        }
        $zip = trim(preg_replace('/[^0-9]/', '', $zip)); // only interested in digits
        if (mb_substr($zip, 0, 6) === '783123') {
            return 'IN-NL';
        }
        /**
         * The source of these look ups are from Wikipedia at
         * the URL
         * http://en.wikipedia.org/wiki/Postal_Index_Number
         * and is licenced under the Creative Commons Attribution-ShareAlike License
         */
        $threedigits = array(
            '744' => 'AN',
            '790' => 'AR', '791' => 'AR', '792' => 'AR',
            '369' => 'DN',
            '403' => 'GA',
            '814' => 'JH', '815' => 'JH', '816' => 'JH', '817' => 'JH', '818' => 'JH', '819' => 'JH', '820' => 'JH',
            '821' => 'JH', '822' => 'JH', '823' => 'JH', '825' => 'JH', '826' => 'JH', '827' => 'JH',
            '828' => 'JH', '829' => 'JH', '830' => 'JH', '831' => 'JH', '832' => 'JH', '833' => 'JH', '834' => 'JH',
            '835' => 'JH',
            '673' => 'LD',
            '795' => 'MN', '796' => 'MN',
            '793' => 'ML', '794' => 'ML',
            '797' => 'NL', '798' => 'NL',
            '605' => 'PY',
            '737' => 'SK',
            '799' => 'TR',
            '246' => 'UL', '247' => 'UL', '248' => 'UL', '249' => 'UL', '250' => 'UL', '251' => 'UL', '252' => 'UL',
            '253' => 'UL', '254' => 'UL', '255' => 'UL', '256' => 'UL', '257' => 'UL', '258' => 'UL', '259' => 'UL',
            '260' => 'UL', '261' => 'UL', '262' => 'UL', '263' => 'UL'
        );
        if (isset($threedigits[mb_substr($zip, 0, 3)]) === TRUE) {
            return 'IN-' . $threedigits[mb_substr($zip, 0, 3)];
        }
        $twodigits = array(
            '50' => 'AP', '51' => 'AP', '52' => 'AP', '53' => 'AP',
            '78' => 'AS',
            '80' => 'BR', '81' => 'BR', '82' => 'BR', '83' => 'BR', '84' => 'BR', '85' => 'BR',
            '16' => 'CH',
            '49' => 'CT',
            '11' => 'DL',
            '36' => 'GJ', '37' => 'GJ', '38' => 'GJ', '39' => 'GJ',
            '12' => 'HR', '13' => 'HR',
            '17' => 'HP',
            '18' => 'JK', '19' => 'JK',
            '56' => 'KA', '57' => 'KA', '58' => 'KA', '59' => 'KA',
            '67' => 'KL', '68' => 'KL', '69' => 'KL',
            '45' => 'MP', '46' => 'MP', '47' => 'MP', '48' => 'MP',
            '40' => 'MH', '41' => 'MH', '42' => 'MH', '43' => 'MH', '44' => 'MH',
            '75' => 'OR', '76' => 'OR', '77' => 'OR',
            '14' => 'PB', '15' => 'PB',
            '30' => 'RJ', '31' => 'RJ', '32' => 'RJ', '33' => 'RJ', '34' => 'RJ',
            '60' => 'TN', '61' => 'TN', '62' => 'TN', '63' => 'TN', '64' => 'TN',
            '20' => 'UP', '21' => 'UP', '22' => 'UP', '23' => 'UP', '24' => 'UP', '25' => 'UP',
            '26' => 'UP', '27' => 'UP', '28' => 'UP',
            '70' => 'WB', '71' => 'WB', '72' => 'WB', '73' => 'WB', '74' => 'WB'
        );
        if (isset($twodigits[mb_substr($zip, 0, 2)]) === TRUE) {
            return 'IN-' . $twodigits[mb_substr($zip, 0, 2)];
        }
        return 'IN';
    }

}
