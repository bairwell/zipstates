<?php
/**
 * The Nigerian zipstates class.
 *
 * This work is licensed under the Creative Commons Attribution 3.0 Unported License.
 * To view a copy of this license, visit
 * http://creativecommons.org/licenses/by/3.0/or send
 * a letter to
 * Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 *
 * The postal/zip codes look ups in this file are based off information on
 * http://www.nipost.gov.ng/PostCode.aspx
 * and cross referenced to
 * http://en.wikipedia.org/wiki/ISO_3166-2:NG
 * and is covered under the Creative Commons Attribution 3.0 Unported License .
 *
 * @package Bairwell
 * @subpackage ZipStates
 * @author Richard Chiswell <richard@Bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license Creative Commons Attribution 3.0 Unported License
 */
namespace Bairwell\ZipStates\Countries;

/**
 * Nigeria
 */
class NG extends Base
{

    /**
     * Data from http://www.nipost.gov.ng/PostCode.aspx
     * cross referenced with http://en.wikipedia.org/wiki/ISO_3166-2:NG
     * @var array The postcode prefixes to counties
     */
    private $statezips = array(
        // Abia
        '440' => 'AB',
        '441' => 'AB',
        '442' => 'AB',
        '451' => 'AB',
        '452' => 'AB',
        '453' => 'AB',
        // Adamawa
        '640' => 'AD',
        '641' => 'AD',
        '642' => 'AD',
        '643' => 'AD',
        '650' => 'AD',
        '651' => 'AD',
        '652' => 'AD',
        // Akwa Ibom
        '520' => 'AK',
        '521' => 'AK',
        '522' => 'AK',
        '523' => 'AK',
        '524' => 'AK',
        '530' => 'AK',
        '531' => 'AK',
        '532' => 'AK',
        '533' => 'AK',
        '534' => 'AK',
        // Anambra
        '420' => 'AN',
        '421' => 'AN',
        '422' => 'AN',
        '423' => 'AN',
        '430' => 'AN',
        '431' => 'AN',
        '432' => 'AN',
        '433' => 'AN',
        '434' => 'AN',
        '435' => 'AN',
        // Bauchi
        '740' => 'BA',
        '741' => 'BA',
        '742' => 'BA',
        '743' => 'BA',
        '750' => 'BA',
        '751' => 'BA',
        '752' => 'BA',
        // Bayelsa
        '560' => 'BY',
        '561' => 'BY',
        '562' => 'BY',
        // Benue
        '970' => 'BE',
        '971' => 'BE',
        '972' => 'BE',
        '973' => 'BE',
        '980' => 'BE',
        '981' => 'BE',
        '982' => 'BE',
        // Borno
        '600' => 'BO',
        '601' => 'BO',
        '602' => 'BO',
        '603' => 'BO',
        '610' => 'BO',
        '611' => 'BO',
        '612' => 'BO',
        // Cross River
        '550' => 'CR',
        '551' => 'CR',
        '552' => 'CR',
        '540' => 'CR',
        '541' => 'CR',
        '542' => 'CR',
        '543' => 'CR',
        // Delta
        '320' => 'DE',
        '321' => 'DE',
        '322' => 'DE',
        '330' => 'DE',
        '331' => 'DE',
        '333' => 'DE',
        '334' => 'DE',
        // Ebonyi
        '480' => 'EB',
        '481' => 'EB',
        '482' => 'EB',
        '490' => 'EB',
        '491' => 'EB',
        // Edo
        '300' => 'ED',
        '301' => 'ED',
        '302' => 'ED',
        '310' => 'ED',
        '311' => 'ED',
        '312' => 'ED',
        '313' => 'ED',
        // Ekiti
        '360' => 'EK',
        '361' => 'EK',
        '362' => 'EK',
        '370' => 'EK',
        '371' => 'EK',
        '372' => 'EK',
        // Enugu
        '400' => 'EN',
        '401' => 'EN',
        '402' => 'EN',
        '410' => 'EN',
        '411' => 'EN',
        '412' => 'EN',
        '413' => 'EN',
        // Gombe
        '760' => 'GO',
        '761' => 'GO',
        '762' => 'GO',
        '770' => 'GO',
        '771' => 'GO',
        // Imo
        '460' => 'IM',
        '461' => 'IM',
        '462' => 'IM',
        '463' => 'IM',
        '464' => 'IM',
        '470' => 'IM',
        '471' => 'IM',
        '472' => 'IM',
        '473' => 'IM',
        '474' => 'IM',
        '475' => 'IM',
        // Jigawa
        '705' => 'JI',
        '720' => 'JI',
        '721' => 'JI',
        '730' => 'JI',
        '731' => 'JI',
        '732' => 'JI',
        '733' => 'JI',
        // Kaduna
        '800' => 'KD',
        '801' => 'KD',
        '802' => 'KD',
        '810' => 'KD',
        '811' => 'KD',
        '812' => 'KD',
        // Kano
        '701' => 'KN',
        '702' => 'KN',
        '703' => 'KN',
        '704' => 'KN',
        '710' => 'KN',
        '711' => 'KN',
        '712' => 'KN',
        '713' => 'KN',
        // Katsina
        '820' => 'KT',
        '821' => 'KT',
        '822' => 'KT',
        '823' => 'KT',
        '824' => 'KT',
        '830' => 'KT',
        '831' => 'KT',
        '832' => 'KT',
        '833' => 'KT',
        // Kebbi
        '860' => 'KE',
        '861' => 'KE',
        '862' => 'KE',
        '863' => 'KE',
        '870' => 'KE',
        '871' => 'KE',
        '872' => 'KE',
        // Kogi
        '260' => 'KO',
        '261' => 'KO',
        '262' => 'KO',
        '263' => 'KO',
        '264' => 'KO',
        '270' => 'KO',
        '271' => 'KO',
        '272' => 'KO',
        // Kwara
        '240' => 'KW',
        '241' => 'KW',
        '242' => 'KW',
        '243' => 'KW',
        '251' => 'KW',
        '252' => 'KW',
        // Lagos
        '100' => 'LA',
        '101' => 'LA',
        '102' => 'LA',
        '103' => 'LA',
        '104' => 'LA',
        '105' => 'LA',
        // Nassarawa
        '950' => 'NA',
        '951' => 'NA',
        '960' => 'NA',
        '961' => 'NA',
        '962' => 'NA',
        // Niger
        '910' => 'NI',
        '911' => 'NI',
        '912' => 'NI',
        '913' => 'NI',
        '920' => 'NI',
        '921' => 'NI',
        '922' => 'NI',
        '923' => 'NI',
        // Ogun
        '110' => 'OG',
        '111' => 'OG',
        '112' => 'OG',
        '120' => 'OG',
        '121' => 'OG',
        '122' => 'OG',
        // Ondo
        '340' => 'ON',
        '341' => 'ON',
        '342' => 'ON',
        '350' => 'ON',
        '351' => 'ON',
        '352' => 'ON',
        // Osun
        '220' => 'OS',
        '221' => 'OS',
        '230' => 'OS',
        '231' => 'OS',
        '232' => 'OS',
        '233' => 'OS',
        // Oyo
        '200' => 'OY',
        '201' => 'OY',
        '202' => 'OY',
        '203' => 'OY',
        '210' => 'OY',
        '211' => 'OY',
        '212' => 'OY',
        // Plateau
        '930' => 'PL',
        '931' => 'PL',
        '932' => 'PL',
        '933' => 'PL',
        '940' => 'PL',
        '941' => 'PL',
        '942' => 'PL',
        // Rivers
        '500' => 'RI',
        '501' => 'RI',
        '502' => 'RI',
        '503' => 'RI',
        '504' => 'RI',
        '510' => 'RI',
        '511' => 'RI',
        '512' => 'RI',
        // Sokoto
        '840' => 'SO',
        '841' => 'SO',
        '842' => 'SO',
        '843' => 'SO',
        '850' => 'SO',
        '851' => 'SO',
        '852' => 'SO',
        '853' => 'SO',
        // Taraba
        '660' => 'TA',
        '661' => 'TA',
        '662' => 'TA',
        '663' => 'TA',
        '670' => 'TA',
        '671' => 'TA',
        '672' => 'TA',
        // Yobe
        '620' => 'YO',
        '621' => 'YO',
        '622' => 'YO',
        '630' => 'YO',
        '631' => 'YO',
        '632' => 'YO',
        // Zamfara
        '880' => 'ZA',
        '881' => 'ZA',
        '882' => 'ZA',
        '883' => 'ZA',
        '890' => 'ZA',
        '891' => 'ZA',
        // FCT-Abuja
        '900' => 'FC',
        '901' => 'FC',
        '902' => 'FC',
        '903' => 'FC',
        '904' => 'FC',
        '905' => 'FC'
    );

    /**
     * Attempts to validate a zip/postal code sent to it
     *
     * @param string $zipcode The zip code being provided
     * @return boolean|string string=formatted zipcode,False=not matched,null=unknown
     */
    public function validateZipcode($zipcode)
    {
        $noSpaces = strtoupper(preg_replace('/[ \,\.\-]/', '', $zipcode));
        if (preg_match('/^[0-9]{6}$/', $noSpaces) === 1) {
            $return = $noSpaces;
        } else {
            $return = FALSE;
        }
        return $return;
    }

    /**
     * Gets the ISO 3166-2 subdivision for this country from the zip/postal code
     *
     * May return:
     *  NULL : Means invalid zip/postal code
     *  xx: Unable to match subdivision of country xx
     *  xx-YYY: The country XX and subdivision YYY
     *
     * @throws \Exception
     * @param string $zip The zipcode/postal code
     * @return string|null NULL=Invalid zipcode for area,string=as much of the subdivision as possible (including the country code)
     */
    public function getStateSubdivisionFromZipcode($zip)
    {
        $zip = $this->validateZipcode($zip);
        if (is_string($zip) === FALSE) {
            return NULL;
        }
        $ziptomatch = mb_substr($zip, 0, 3);
        if (isset($this->statezips[$ziptomatch]) === TRUE) {
            return 'NG-' . $this->statezips[$ziptomatch];
        }
        return 'NG';
    }

}
