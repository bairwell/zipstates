<?php
/**
 * The ZipSates factory class.
 *
 * This work is licensed under the Creative Commons Attribution 3.0 Unported License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/3.0/ or send
 * a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 *
 * @package Bairwell
 * @subpackage ZipStates
 * @author Richard Chiswell <richard@Bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license Creative Commons Attribution 3.0 Unported License
 */
namespace Bairwell\ZipStates;

/**
 * A basic factory class to enable Unittesting in the future
 */
class Factory extends \Bairwell\Factory
{
    /**
     * Set our class prefix
     * @static
     * @return string The class prefix
     */
    protected static function getClassPrefix()
    {
        return 'ZipStates';
    }
}
