<?php
/**
 * The main ZipStates class.
 *
 * This work is licensed under the Creative Commons Attribution 3.0 Unported License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/3.0/ or send
 * a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 *
 * @package Bairwell
 * @subpackage ZipStates
 * @author Richard Chiswell <richard@Bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license Creative Commons Attribution 3.0 Unported License
 */
namespace Bairwell\ZipStates;

/**
 * Validates zip codes and locates states. Use this class if you don't want to instantiate
 * different objects for all the different countries you are testing
 * @throws \Exception
 */
class ZipStates
{

    /**
     * Validates a postal code
     *
     * If the postal/zipcode is "obviously" incorrect for the country (for example, letters in an
     * American postal code, or "FRED" for a GB one), then this will return FALSE. If it does "look"
     * valid, it will format it to the expected standard for that country (so "le93bh" in GB
     * will return 'LE9 3BH'). If we can't deal with the country yet, NULL is returned (as it is neither
     * valid or invalid)
     *
     * @param string $country The two character country code
     * @param string $zipcode The postal/zip code
     * @return bool|null|string NULL if unable to valid, FALSE if incorrect, string if postal code
     */
    public function validateZipcode($country, $zipcode)
    {
        $this->validate($country, $zipcode);
        $return = NULL;
        $library = $this->getLibrary($country);
        if ($library !== NULL) {
            $return = $library->validateZipcode($zipcode);
        }
        return $return;
    }

    /**
     * Gets the appropriate library
     *
     * @param string $country The two letter country code
     * @return \Bairwell\ZipStates\Countries\Base|NULL
     */
    private function getLibrary($country)
    {
        $library = NULL;
        switch ($country) {
            case 'GB':
                /**
                 * @var \Bairwell\ZipStates\Countries\GB $library
                 */
                $library = \Bairwell\ZipStates\Factory::getLibrary('Countries\GB');
                break;
            case 'AU':
                /**
                 * @var \Bairwell\ZipStates\Countries\AU $library
                 */
                $library = \Bairwell\ZipStates\Factory::getLibrary('Countries\AU');
                break;
            case 'CA':
                /**
                 * @var \Bairwell\ZipStates\Countries\CA $library
                 */
                $library = \Bairwell\ZipStates\Factory::getLibrary('Countries\CA');
                break;
            case 'IN':
                /**
                 * @var \Bairwell\ZipStates\Countries\IN $library
                 */
                $library = \Bairwell\ZipStates\Factory::getLibrary('Countries\IN');
                break;
            case 'NG':
                /**
                 * @var \Bairwell\ZipStates\Countries\NG $library
                 */
                $library = \Bairwell\ZipStates\Factory::getLibrary('Countries\NG');
                break;
            case 'NZ':
                /**
                 * @var \Bairwell\ZipStates\Countries\NZ $library
                 */
                $library = \Bairwell\ZipStates\Factory::getLibrary('Countries\NZ');
                break;
            case 'US':
                /**
                 * @var \Bairwell\ZipStates\Countries\US $library
                 */
                $library = \Bairwell\ZipStates\Factory::getLibrary('Countries\US');
                break;
        }//end switch
        return $library;
    }

    /**
     * Gets the appropriate state/region subdivision by an zipcode
     *
     * @param string $country The two character country code
     * @param string $zipcode The postal/zip code
     * @return string|null The subdivision or NULL if unable to match
     */
    public function getStateSubdivisionByZipcode($country, $zipcode)
    {
        $this->validate($country, $zipcode);
        $return = NULL;
        $library = $this->getLibrary($country);
        if ($library !== NULL) {
            $return = $library->getStateSubdivisionFromZipcode($zipcode);
        }
        return $return;
    }

    /**
     * Validates the variables sent
     *
     * @throws \Exception
     * @param string $country The ISO country code
     * @param string $zipcode The postal/zip code
     * @return void
     */
    private function validate($country, $zipcode)
    {
        if (is_string($country) === FALSE) {
            throw new \Exception('Invalid country code');
        }
        if (mb_strlen($country) !== 2) {
            throw new \Exception('Invalid country code (wrong length)');
        }
        if (is_string($zipcode) === FALSE && is_numeric($zipcode) === FALSE) {
            throw new \Exception('Invalid postal/zip code');
        }
    }

    /**
     * Gets the appropriate country subdivision by an zipcode
     *
     * Only really useful for GB which is made up of multiple constituent parts, but some
     * zip codes (such as the 00601 US zip code which is actually Puerto Rico) map to other countries
     *
     * @param string $country The two character country code
     * @param string $zipcode The postal/zip code
     * @return string|null The subdivision or NULL if unable to match
     */
    public function getCountrySubdivisionByZipcode($country, $zipcode)
    {
        $this->validate($country, $zipcode);
        $return = $country;
        $library = $this->getLibrary($country);
        if ($library !== NULL) {
            $return = $library->getCountrySubdivisionFromZipcode($zipcode);
        }
        return $return;
    }
}
