<?php
/**
 * Demonstrates the ZipStates system.
 *
 * @package Bairwell
 * @subpackage ZipStates
 * @author Richard Chiswell <richard@Bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license Creative Commons Attribution 3.0 Unported License
 */
require 'BairwellBootstrap.php';

$areas = array(
    'GB' => array('LE9 3BH', 'WC1E 7HJ', 'TN23 4GL'),
    'US' => array('90210')
);

/**
 * @var \Bairwell\ZipStates\ZipStates $zipstates
 */
$zipstates = \Bairwell\ZipStates\Factory::getLibrary('ZipStates');
foreach ($areas as $country => $zipcodes) {
    foreach ($zipcodes as $zipcode) {
        echo 'The postal/zip code of ' . $zipcode . ' in ' . $country . ' maps to the area ';
        echo $zipstates->getStateSubdivisionByZipcode($country, $zipcode) . ' in ';
        echo $zipstates->getCountrySubdivisionByZipcode($country, $zipcode) . "\n";
    }
}
