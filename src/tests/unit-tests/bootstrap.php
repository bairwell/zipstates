<?php
/**
 * This is the main bootstrap used by Bairwell test items
 *
 * This work is licensed under the Creative Commons Attribution 3.0 Unported License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/3.0/ or send
 * a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 *
 * @package Bairwell
 * @subpackage Autoloader
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license Creative Commons Attribution 3.0 Unported License
 */

/**
 * PHP_MAJOR_VERSION isn't defined until 5.2.7, so if it isn't defined, we know we're running on an old version
 */
if (defined('PHP_MAJOR_VERSION') === FALSE) {
    die('We require PHP 5.3.0 at a minimum as we use namespaces and late static bindings. You have PHP ' . phpversion(
    ));
}
if (PHP_MAJOR_VERSION === 5 && PHP_MINOR_VERSION < 3) {
    die('We require PHP 5.3.0 at a minimum as we use namespaces and late static bindings. You have PHP ' . phpversion(
    ));
}
if (defined('MB_OVERLOAD_STRING') === FALSE) {
    die('We require that PHP has mbstring installed: http://php.net/mbstring.installation.php');
}
/**
 * We use UTF-8 encoding
 */
mb_internal_encoding('UTF-8');

/**
 * Load the autoloader
 */
#set_include_path('.' . PATH_SEPARATOR . get_include_path());
$dirname = dirname(__FILE__);
include 'Bairwell/Autoloader.php';
$autoloader = new \Bairwell\Autoloader();
spl_autoload_register(array($autoloader, 'load'));

$autoloader->addPrefix('Bairwell\\Zipstates\\Tests', $dirname . '/php');
$autoloader->addPrefix('Bairwell', $dirname . '/../../php/Bairwell/');
