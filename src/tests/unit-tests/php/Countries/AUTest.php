<?php
/**
 * Tests the Australian code.
 *
 * This work is licensed under the Creative Commons Attribution 3.0 Unported License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/3.0/ or send
 * a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 *
 * @package Bairwell
 * @subpackage ZipStates
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license Creative Commons Attribution 3.0 Unported License
 */
namespace Bairwell\Tests\ZipStates\Countries;

/**
 * Australia
 */
class AUTest extends Base
{

    /**
     * Check the validate zipcode system returns appropriate results
     *
     * @return void
     */
    public function testValidateZipcode()
    {
        $goodZips = array(
            'NSW 2000' => '2000', # Sydney Opera House
            'act 2617' => '2617', # Westfield Belconnen
            'wa6018' => '6018', # Westfield Innaloo
            'nsw2009' => '2009', # foxsports.com.au
            '8060' => '8060' # macquarie
        );
        $badZips = array(
            'LE9 3BH', # GB
            '90210', # US
            'NS 2000', # Wrong length
            '', # null
            '1', # wrong length
            '12', # wrong length
            '123', # Wrong length
            '12345', # wrong length
            'nws2009' # misspelling
        );
        /**
         * @var Bairwell\ZipStates\Countries\AU $subdivisionService
         */
        $subdivisionService = \Bairwell\ZipStates\Factory::getLibrary('Countries\AU');
        foreach ($goodZips as $zip => $correctlyFormatted) {
            $this->assertEquals($correctlyFormatted, $subdivisionService->validateZipcode($zip));
        }
        foreach ($badZips as $zip) {
            $this->assertFalse($subdivisionService->validateZipcode($zip));
        }
    }

    /**
     * Tests that we get the appropriate states back from our zipcodes
     *
     * @return void
     */
    public function testGetState()
    {
        /**
         * @var Bairwell\ZipStates\Countries\AU $subdivisionService
         */
        $subdivisionService = \Bairwell\ZipStates\Factory::getLibrary('Countries\AU');
        /**
         * First random checks
         */
        $this->assertEquals('AU-NSW', $subdivisionService->getStateSubdivisionFromZipcode('NSW 2009'));
        $this->assertEquals('AU', $subdivisionService->getCountrySubdivisionFromZipcode('NSW 2009'));

        $this->assertEquals('AU-ACT', $subdivisionService->getStateSubdivisionFromZipcode('act2617'));
        $this->assertEquals('AU', $subdivisionService->getCountrySubdivisionFromZipcode('act2617'));

        $this->assertEquals('AU-WA', $subdivisionService->getStateSubdivisionFromZipcode('6018'));
        $this->assertEquals('AU', $subdivisionService->getCountrySubdivisionFromZipcode('6018'));

        $this->assertNull($subdivisionService->getStateSubdivisionFromZipcode('Invalid'));
        $this->assertNull($subdivisionService->getCountrySubdivisionFromZipcode('Invalid'));

        /**
         * Looks correct, but doesn't match to a region
         */
        $this->assertEquals('AU', $subdivisionService->getStateSubdivisionFromZipcode('nsw0000'));
        $this->assertEquals('AU', $subdivisionService->getCountrySubdivisionFromZipcode('nsw0000'));
        /**
         * Now special regions checks
         */
        $this->assertEquals('NF', $subdivisionService->getStateSubdivisionFromZipcode('2899'));
        $this->assertEquals('NF', $subdivisionService->getCountrySubdivisionFromZipcode('2899'));
        $this->assertEquals('CX', $subdivisionService->getStateSubdivisionFromZipcode('6798'));
        $this->assertEquals('CX', $subdivisionService->getCountrySubdivisionFromZipcode('6798'));
        $this->assertEquals('CC', $subdivisionService->getStateSubdivisionFromZipcode('6799'));
        $this->assertEquals('CC', $subdivisionService->getCountrySubdivisionFromZipcode('6799'));
        /**
         * If all is still valid, let's plough through our database of records
         */
        $array = $this->loadCommentedCSV('AU.csv');
        /**
         * These are suburbs near state/territory borders which Australia Post have coded
         * in a different state. The CSV file does have the "correct state", but our detection
         * method does not deal with these edge cases
         *
         * Format is:
         *  [zipcode] => Expected zip code as returned from getStateSubdivisionFromZipcode
         */
        $overrides = array(
            '0872' => 'AU-NT',
            '2406' => 'AU-NSW',
            '2540' => 'AU-NSW',
            '2611' => 'AU-ACT',
            '2618' => 'AU-ACT',
            '2620' => 'AU-ACT',
            '2899' => 'NF', # Norfolk Island
            '3644' => 'AU-VIC',
            '3691' => 'AU-VIC',
            '3707' => 'AU-VIC',
            '4375' => 'AU-QLD',
            '4377' => 'AU-QLD',
            '4380' => 'AU-QLD',
            '4383' => 'AU-QLD',
            '4385' => 'AU-QLD',
            '4825' => 'AU-QLD',
            '6798' => 'CX', # Christmas Island
            '6799' => 'CC', # Cocos (Keeling) Islands
            '7151' => 'AU-TAS' # The Antarctic bases share a postcode with Macquarie Island (part of Tasmania)
        );
        foreach ($array as $line) {
            $region = $line[1];
            $zip = $line[0];
            $area = $line[2];
            $result = $subdivisionService->getStateSubdivisionFromZipcode($zip);
            $message = 'Checking ' . $zip . ' for ' . $region;
            $expected = 'AU-' . $area;
            if (isset($overrides[$zip])) {
                $this->assertEquals($overrides[$zip], $result, $message . ' (overridden)');
                if (in_array($overrides[$zip], array('NF', 'CX', 'CC'))) {
                    $this->assertEquals($overrides[$zip], $subdivisionService->getCountrySubdivisionFromZipcode($zip), $message . ' (overridden)');
                }
                else {
                    $this->assertEquals('AU', $subdivisionService->getCountrySubdivisionFromZipcode($zip), $message . ' (overridden)');
                }
            }
            else {
                $this->assertEquals($expected, $result, $message);
                $this->assertEquals('AU', $subdivisionService->getCountrySubdivisionFromZipcode($zip), $message);
            }
        }
    }
}