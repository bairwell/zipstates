<?php
/**
 * Base class for testing.
 *
 * This work is licensed under the Creative Commons Attribution 3.0 Unported License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/3.0/ or send
 * a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 *
 * @package Bairwell
 * @subpackage ZipStates
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license Creative Commons Attribution 3.0 Unported License
 */
namespace Bairwell\Tests\ZipStates\Countries;

/**
 * Creates a base class for all of our unit tests
 * @throws \Exception
 */
abstract class Base extends \PHPUnit_Framework_TestCase
{

    /**
     * Gets a fixture file from the storage
     *
     * @throws \Exception
     * @param string $inputFile The name of the file
     * @return string The fully qualified path for loading
     */
    protected function getFixtureFile($inputFile)
    {
        $filename = realpath(__DIR__ . '/../Fixtures/' . $inputFile);
        if (!file_exists($filename) || !is_readable($filename)) {
            throw new \Exception('Unable to find fixture file');
        }
        return $filename;
    }

    /**
     * Loads a # commented csv file
     *
     * @throws \Exception
     * @param string $inputFile The name of the file
     * @param string $delimiter The delimiter of the csv (usually comma)
     * @return array An array of the lines
     */
    protected function loadCommentedCSV($inputFile, $delimiter = ',')
    {
        $filename = $this->getFixtureFile($inputFile);
        $array = array();
        if (($handle = fopen($filename, 'r')) !== FALSE) {
            while (($buffer = fgets($handle, 4096)) !== false) {
                if ($buffer !== NULL && $buffer[0] !== '#' && mb_strlen($buffer) > 3) {
                    $line = str_getcsv($buffer, $delimiter);
                    $array[] = $line;
                }
            }
            if (!feof($handle)) {
                throw new \Exception('Unable to read fixture file');
            }
        }
        else {
            throw new \Exception('Unable to fopen fixture file');
        }
        fclose($handle);
        return $array;
    }

    /**
     * All of our tests must support a vlaidate zipcode
     *
     * @abstract
     * @return void
     */
    abstract public function testValidateZipcode();

}