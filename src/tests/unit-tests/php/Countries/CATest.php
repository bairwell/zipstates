<?php
/**
 * Tests the Canadian code.
 *
 * This work is licensed under the Creative Commons Attribution 3.0 Unported License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/3.0/ or send
 * a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 *
 * @package Bairwell
 * @subpackage ZipStates
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license Creative Commons Attribution 3.0 Unported License
 */
namespace Bairwell\Tests\ZipStates\Countries;

/**
 * Tests the Canadian zip codes
 */
class CATest extends Base
{

    /**
     * Check the validate zipcode system returns appropriate results
     *
     * @return void
     */
    public function testValidateZipcode()
    {
        $goodZips = array(
            'L5S 1Z9' => 'L5S 1Z9', # Air Canada Training Location in Toronto
            'v7b1k3' => 'V7B 1K3', # Air Canada Training Location in Vancouver
            'n6A5 c7' => 'N6A 5C7', # Blackburn Radio Inc
            'H0H0H0' => 'H0H 0H0' # Santa Claus's postcode LOOKS valid, but isn't actually
        );
        $badZips = array(
            'ACT 2617', # Australia',
            'LE9 3BH', # GB
            '90210', # US
            'W5S 1Z9', # No forward sorting areas with W as a prefix
            'L5D 1Z9', # D is not a permitted letter
            'L5F 1Z9', # F is not a permitted letter
            'L5I 1Z9', # D is not a permitted letter
            'L5O 1Z9', # O is not a permitted letter
        );
        /**
         * @var Bairwell\ZipStates\Countries\CA $subdivisionLibrary
         */
        $subdivisionLibrary = \Bairwell\ZipStates\Factory::getLibrary('Countries\CA');
        foreach ($goodZips as $zip => $correctlyFormatted) {
            $this->assertEquals($correctlyFormatted, $subdivisionLibrary->validateZipcode($zip));
        }
        foreach ($badZips as $zip) {
            $this->assertFalse($subdivisionLibrary->validateZipcode($zip));
        }
    }

    /**
     * Tests that we get the appropriate states back from our zipcodes
     *
     * @return void
     */
    public function testGetState()
    {
        /**
         * @var Bairwell\ZipStates\Countries\CA $subdivisionLibrary
         */
        $subdivisionLibrary = \Bairwell\ZipStates\Factory::getLibrary('Countries\CA');
        $result = $subdivisionLibrary->getStateSubdivisionFromZipcode('ABC');
        $this->assertNull($result, 'Checking for invalid zipcode');
        /**
         * If all is still valid, let's plough through our database of records
         */
        $array = $this->loadCommentedCSV('CA.csv');
        foreach ($array as $line) {
            $zip = $line[0];
            $area = $line[1];
            $result = $subdivisionLibrary->getStateSubdivisionFromZipcode($zip);
            $this->assertEquals($area, $result, 'Testing ' . $zip . ' for state - expecting ' . $area);
            $result = $subdivisionLibrary->getCountrySubdivisionFromZipcode($zip);
            $this->assertEquals('CA', $result, 'Testing ' . $zip . ' for country');
        }
    }

}