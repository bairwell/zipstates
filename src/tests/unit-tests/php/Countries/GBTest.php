<?php
/**
 * Tests the Great Britian code.
 *
 * This work is licensed under the Creative Commons Attribution 3.0 Unported License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/3.0/ or send
 * a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 *
 * @package Bairwell
 * @subpackage ZipStates
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license Creative Commons Attribution 3.0 Unported License
 */
namespace Bairwell\Tests\ZipStates\Countries;

class GBTest extends Base
{
    /**
     * Check the validate zipcode system returns appropriate results
     *
     * @return void
     */
    public function testValidateZipcode()
    {
        $goodZips = array(
            'le93bh' => 'LE9 3BH',
            'LE4 2GX' => 'LE4 2GX',
            'sE84jL' => 'SE8 4JL',
            'TN234GL' => 'TN23 4GL',
            'WC1e7 HJ' => 'WC1E 7HJ',
            'ze21aa' => 'ZE2 1AA',
            'm139pl' => 'M13 9PL',
            'SW1A1AA' => 'SW1A 1AA', # Buckingham Palace
            'GIR0AA' => 'GIR 0AA', # Girobank
            'BX11LT' => 'BX1 1LT', # Banks/VAT
            'Bfpo1234' => 'BFPO 1234', # British Forces
            'BfpoC/O 123' => 'BFPO c/o 123', # British Forces
            'santa1' => 'SAN TA1', # Santa
            'AI-2640' => 'AI-2640', # Anguilla
            'ASCN1ZZ' => 'ASCN 1ZZ', # Ascension Island
            'BBND 1ZZ' => 'BBND 1ZZ', # British Indian Ocean Territory
            'BIQQ1ZZ' => 'BIQQ 1ZZ', # British Antarctic Territory
            'FIQQ1ZZ' => 'FIQQ 1ZZ', # Falkland Islands
            'GX111AA' => 'GX11 1AA', # Gibraltar
            'PCRN1ZZ' => 'PCRN 1ZZ', # Pitcairn Islands
            'SIQQ1ZZ' => 'SIQQ 1ZZ', # South Georgia and the South Sandwich Islands
            'STHL1zz' => 'STHL 1ZZ', # Saint Helena
            'tdcu1z z' => 'TDCU 1ZZ', # Tristan da Cunha
            'tkca1ZZ' => 'TKCA 1ZZ' # Turks and Caicos Islands
        );
        $badZips = array(
            'd7 1aa', # Old Dublin style
            'NOR 07A', # Old Norwich style (1960)
            'le93ca', # C of CIKMOV is not allowed in inbound
            'M139pm', # M of CIKMOV is not allowed in inbound
            'NSW 2000', # Australia
            '90210', # US
            'LE93B', # Wrong length
            'SEB4JL', # Letter instead of number
            'TN23 4GLZ' # Wrong length
        );
        /**
         * @var Bairwell\ZipStates\Countries\GB $subdivisionLibrary
         */
        $subdivisionLibrary = \Bairwell\ZipStates\Factory::getLibrary('Countries\GB');
        foreach ($goodZips as $zip => $correctlyFormatted) {
            $this->assertEquals($correctlyFormatted, $subdivisionLibrary->validateZipcode($zip));
        }
        foreach ($badZips as $zip) {
            $this->assertFalse($subdivisionLibrary->validateZipcode($zip));
        }
    }

    /**
     * Tests that we get the appropriate states back from our zipcodes
     *
     * @return void
     */
    public function testGetState()
    {
        /**
         * @var Bairwell\ZipStates\Countries\GB $subdivisionLibrary
         */
        $subdivisionLibrary = \Bairwell\ZipStates\Factory::getLibrary('Countries\GB');
        /**
         * First random checks
         */

        $this->assertEquals('GB-LCE', $subdivisionLibrary->getStateSubdivisionFromZipcode('LE9 3BH'));
        $this->assertEquals('GB-LND', $subdivisionLibrary->getStateSubdivisionFromZipcode('SE8 4JL'));
        $this->assertNull($subdivisionLibrary->getStateSubdivisionFromZipcode('Invalid'));
        /**
         * This looks like a valid postcode, but doesn't actually map to anywhere
         */
        $this->assertNull($subdivisionLibrary->getStateSubdivisionFromZipcode('GX000AA'));
        /**
         * Looks correct, but doesn't match to a region
         */
        $this->assertEquals('GB', $subdivisionLibrary->getStateSubdivisionFromZipcode('BX991AA'));
        /**
         * Now special regions checks
         */
        $this->assertEquals('IM', $subdivisionLibrary->getStateSubdivisionFromZipcode('IM12AR'));
        /**
         * Now test the country subdivisions
         */
        $array = array(
            'GB-ENG' => array('LE93BH', 'se84jl', 'tn234gl', 'wc1E7hj', 'm139pl'),
            'GB-SCT' => array('G24JL', 'eh36qn', 'ab21 0GL', 'PA345ru'),
            'GB-WLS' => array('ll145dg', 'LL14 4EH', 'cf30 3ey', 'CF3 5WL'),
            'GB-NIR' => array('BT11NB', 'BT2 8BG', 'BT30 7TP', 'BT32 3UR'),
            'IM' => array('IM12AR', 'im1 3AD', 'IM1 1EQ', 'IM992QS'),
            'JE' => array('je48ns'),
            'GG' => array('GY9 3UF', 'GY1 3HG'),
            'GB' => array('SANta1', 'BX1 1LT', 'bx55at', 'GIR0AA'),
            'AI' => array('Ai2640'),
            'AC' => array('ASCN1zz'),
            'IO' => array('BBND1ZZ'),
            'AQ' => array('BIQQ1ZZ'),
            'FK' => array('FIQQ1ZZ'),
            'GI' => array('gx111aa'),
            'PN' => array('pcrn1zz'),
            'GS' => array('siQQ1ZZ'),
            'SH' => array('STHL1ZZ', 'tdcu1zz'),
            'TC' => array('TKca1zz')
        );
        foreach ($array as $expected => $postcodes) {
            foreach ($postcodes as $postcode) {
                $this->assertEquals($expected, $subdivisionLibrary->getCountrySubdivisionFromZipcode($postcode), $postcode);
            }
        }
        /**
         * If all is still valid, let's plough through our database of records
         */
        $array = $this->loadCommentedCSV('GB.csv');
        foreach ($array as $line) {
            $region = $line[1];
            $zip = $line[0];
            $country = $line[2];
            if (isset($line[3])) {
                $area = $line[3];
            }
            else {
                $area = '(Generated ' . $region . ' / ' . $zip . ' ' . $country . ')';
            }
            $result = $subdivisionLibrary->getStateSubdivisionFromZipcode($zip);
            $message = 'Checking ' . $zip . ' for ' . $region . ' (' . $area . ')';
            $this->assertEquals($region, $result, $message);
            $result = $subdivisionLibrary->getCountrySubdivisionFromZipcode($zip);
            $this->assertEquals($country, $result, $message);
        }
    }

    /**
     * Tests that we get the appropriate country back from our zipcodes
     *
     * @return void
     */
    public function testGetCountrySubdivisionFromState()
    {
        /**
         * @var Bairwell\ZipStates\Countries\GB $subdivisionLibrary
         */
        $subdivisionLibrary = \Bairwell\ZipStates\Factory::getLibrary('Countries\GB');
        /**
         * First random checks
         */

        $this->assertEquals('GB-ENG', $subdivisionLibrary->getCountrySubdivisionFromState('GB-LND'));
        $this->assertEquals('GB-NIR', $subdivisionLibrary->getCountrySubdivisionFromState('GB-BFS'));
        $emess = NULL;
        try {
            $subdivisionLibrary->getCountrySubdivisionFromState('GB-XXX');
        } catch (\Exception $e) {
            $emess = $e->getMessage();
        }
        $this->assertEquals($emess, 'Unrecognised subdivision of XXX');
        $emess = NULL;
        try {
            $subdivisionLibrary->getCountrySubdivisionFromState('InvalidStateName');
        } catch (\Exception $e) {
            $emess = $e->getMessage();
        }
        $emess = NULL;
        try {
            $subdivisionLibrary->getCountrySubdivisionFromState('XX-ABC');
        } catch (\Exception $e) {
            $emess = $e->getMessage();
        }
        $this->assertEquals($emess, 'Can only deal with GB states');
    }

}