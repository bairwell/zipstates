<?php
/**
 * Tests the Indian code.
 *
 * This work is licensed under the Creative Commons Attribution 3.0 Unported License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/3.0/ or send
 * a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 *
 * @package Bairwell
 * @subpackage ZipStates
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license Creative Commons Attribution 3.0 Unported License
 */
namespace Bairwell\Tests\ZipStates\Countries;

/**
 * India
 */
class INTest extends Base
{
    /**
     * Check the validate zipcode system returns appropriate results
     *
     * @return void
     */
    public function testValidateZipcode()
    {
        $goodZips = array(
            '123456' => '123456',
            '654321' => '654321'
        );
        $badZips = array(
            'ACT 2617', # Australia',
            'LE9 3BH', # GB
            '12345', # must be six digits long
            '0123456', # Cannot start with 0

        );
        /**
         * @var Bairwell\ZipStates\Countries\IN $subdivisionLibrary
         */
        $subdivisionLibrary = \Bairwell\ZipStates\Factory::getLibrary('Countries\IN');
        foreach ($goodZips as $zip => $correctlyFormatted) {
            $this->assertEquals($correctlyFormatted, $subdivisionLibrary->validateZipcode($zip));
        }
        foreach ($badZips as $zip) {
            $this->assertFalse($subdivisionLibrary->validateZipcode($zip));
        }
    }


    /**
     * Tests that we get the appropriate states back from our zipcodes
     *
     * @return void
     */
    public function testGetState()
    {
        /**
         * @var Bairwell\ZipStates\Countries\IN $subdivisionLibrary
         */
        $subdivisionLibrary = \Bairwell\ZipStates\Factory::getLibrary('Countries\IN');
        $result = $subdivisionLibrary->getStateSubdivisionFromZipcode('invalid');
        $this->assertNull($result, 'Checking invalid postcode');
        /**
         * If all is still valid, let's plough through our database of records
         */
        $array = $this->loadCommentedCSV('IN.csv');
        foreach ($array as $line) {
            $region = $line[1];
            $zip = $line[0];
            if (isset($line[2])) {
                $area = $line[2];
            }
            else {
                $area = '';
            }
            $result = $subdivisionLibrary->getStateSubdivisionFromZipcode($zip);
            $this->assertEquals($region, $subdivisionLibrary->getStateSubdivisionFromZipcode($zip), 'Checking ' . $zip . ' for ' . $region . ' (' . $area . ')');
            $this->assertEquals('IN', $subdivisionLibrary->getCountrySubdivisionFromZipcode($zip), 'Checking country ' . $zip . ' for ' . $region . ' (' . $area . ')');
        }

    }

}