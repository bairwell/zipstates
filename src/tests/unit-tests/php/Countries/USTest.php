<?php
/**
 * Tests the United States Code.
 *
 * This work is licensed under the Creative Commons Attribution 3.0 Unported License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/3.0/ or send
 * a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 *
 * @package Bairwell
 * @subpackage ZipStates
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license Creative Commons Attribution 3.0 Unported License
 */
namespace Bairwell\Tests\ZipStates\Countries;

class USTest extends Base
{
    /**
     * Check the validate zipcode system returns appropriate results
     *
     * @return void
     */
    public function testValidateZipcode()
    {
        $goodZips = array(
            '58001',
            '20037',
            '20037-8001',
            '60612-0344',
            '20896-0052',
            '55416',
            '33701-4313'
        );
        $badZips = array(
            'ACT 2617', # Australia
            'LE9 3BH', # GB
            '1',
            '12',
            '123',
            '1234',
            '123456',
            '1234567',
            '12345678',
            '1234567890',
            'abc'
        );
        /**
         * @var Bairwell\ZipStates\Countries\US $subdivisionLibrary
         */
        $subdivisionLibrary = \Bairwell\ZipStates\Factory::getLibrary('Countries\US');
        foreach ($goodZips as $zip) {
            $this->assertEquals($zip, $subdivisionLibrary->validateZipcode($zip));
        }
        foreach ($badZips as $zip) {
            $this->assertFalse($subdivisionLibrary->validateZipcode($zip));
        }
    }

    /**
     * Tests that we get the appropriate states back from our zipcodes
     *
     * @return void
     */
    public function testGetState()
    {
        /**
         * @var Bairwell\ZipStates\Countries\US $subdivisionLibrary
         */
        $subdivisionLibrary = \Bairwell\ZipStates\Factory::getLibrary('Countries\US');
        $result = $subdivisionLibrary->getStateSubdivisionFromZipcode('invalid');
        $this->assertNull($result, 'Checking invalid postcode');
        /**
         * If all is still valid, let's plough through our database of records
         */
        $array = $this->loadCommentedCSV('US.csv');
        foreach ($array as $line) {
            $region = $line[1];
            $zip = $line[0];
            if (isset($line[2])) {
                $area = $line[2];
            }
            else {
                $area = '';
            }
            $result = $subdivisionLibrary->getStateSubdivisionFromZipcode($zip);
            $this->assertEquals($region, $subdivisionLibrary->getStateSubdivisionFromZipcode($zip), 'Checking ' . $zip . ' for ' . $region . ' (' . $area . ')');
            if (in_array(mb_substr($zip, 0, 3), array('006', '007', '009'))) {
                $this->assertEquals('PR', $subdivisionLibrary->getCountrySubdivisionFromZipcode($zip), 'Checking country ' . $zip . ' for ' . $region . ' (' . $area . ')');
            }
            else {
                $this->assertEquals('US', $subdivisionLibrary->getCountrySubdivisionFromZipcode($zip), 'Checking country ' . $zip . ' for ' . $region . ' (' . $area . ')');
            }
        }

    }
}