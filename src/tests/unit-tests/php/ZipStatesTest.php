<?php
/**
 * Tests the main ZipStates class
 *
 * This work is licensed under the Creative Commons Attribution 3.0 Unported License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/3.0/ or send
 * a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 *
 * @package Bairwell
 * @subpackage ZipStates
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license Creative Commons Attribution 3.0 Unported License
 */
namespace Bairwell\Tests\ZipStates;

/**
 * Tests the main zip states class
 */
class ZipStatesTest extends \PHPUnit_Framework_TestCase

{
    /**
     * Check the validate zipcode system returns appropriate results
     *
     * @return void
     */
    public function testValidateZipcode()
    {
        $array = array(
            'AU' => array('6798' => '6798',
                          '6799' => '6799',
                          '4385' => '4385',
                          'NSW 2009' => '2009'),
            'CA' => array('A2A 0A0' => 'A2A 0A0',
                          'A1Y0A0' => 'A1Y 0A0',
                          'b1a0B0' => 'B1A 0B0',
                          'y0b0a0' => 'Y0B 0A0'),
            'GB' => array('LE9 3BH' => 'LE9 3BH',
                          'TN23 4GL' => 'TN23 4GL',
                          'G2 4JL' => 'G2 4JL',
                          'IM1 2AR' => 'IM1 2AR',
                          'SANTA1' => 'SAN TA1',
                          'invalid' => FALSE),
            'IN' => array('560052' => '560052',
                          '226018' => '226018',
                          '109000' => '109000',
                          '10900' => FALSE,
                          '110000' => '110000',
                          '175000' => '175000'),
            'NG' => array('105000' => '105000',
                          '118123' => '118123',
                          '221987' => '221987',
                          '231555' => '231555'),
            'NZ' => array('2145' => '2145',
                          '3178' => '3178',
                          '8024' => '8024',
                          '2112' => '2112'),
            'US' => array('02115' => '02115',
                          '10001' => '10001',
                          '00601' => '00601',
                          '01001' => '01001'),
            'BE' => array('123' => NULL),
            'SE' => array('13104' => NULL),
        );
        /**
         * @var \Bairwell\Zipstates\ZipStates $zipstates
         */
        $zipstates = \Bairwell\ZipStates\Factory::getLibrary('ZipStates');
        foreach ($array as $country => $data) {
            foreach ($data as $zipcode => $expected) {
                $result = $zipstates->validateZipcode($country, $zipcode);
                if (is_string($expected)) {
                    $this->assertEquals($expected, $result, 'When testing ' . $zipcode . ' in ' . $country);
                } else if ($expected === NULL) {
                    $this->assertNull($result, 'When testing ' . $zipcode . ' in ' . $country);
                } else if ($expected === FALSE) {
                    $this->assertFalse($result, 'When testing ' . $zipcode . ' in ' . $country);
                }
            }
        }
    }

    /**
     * Checks for non-string country codes
     *
     * @return void
     */
    public function testExceptionInvalidCountry()
    {
        $emess = NULL;
        /**
         * @var \Bairwell\Zipstates\ZipStates $zipstates
         */
        $zipstates = \Bairwell\ZipStates\Factory::getLibrary('ZipStates');
        try {
            $zipstates->validateZipcode(123, 'ABC');
        } catch (\Exception $e) {
            $emess = $e->getMessage();
        }
        $this->assertEquals($emess, 'Invalid country code');
    }

    /**
     * Checks for wrong lengthed country codes
     *
     * @return void
     */
    public function testExceptionInvalidCountryCodeLength()
    {
        $emess = NULL;
        /**
         * @var \Bairwell\Zipstates\ZipStates $zipstates
         */
        $zipstates = \Bairwell\ZipStates\Factory::getLibrary('ZipStates');
        try {
            $zipstates->validateZipcode('ABC', 'ABC');
        } catch (\Exception $e) {
            $emess = $e->getMessage();
        }
        $this->assertEquals($emess, 'Invalid country code (wrong length)');
    }

    /**
     * Checks for wrong type of postal/zip codes
     *
     * @return void
     */
    public function testExceptionInvalidPostalZip()
    {
        $emess = NULL;
        /**
         * @var \Bairwell\Zipstates\ZipStates $zipstates
         */
        $zipstates = \Bairwell\ZipStates\Factory::getLibrary('ZipStates');
        try {
            $zipstates->validateZipcode('GB', NULL);
        } catch (\Exception $e) {
            $emess = $e->getMessage();
        }
        $this->assertEquals($emess, 'Invalid postal/zip code');
    }

    /**
     * Tests that we get the appropriate states back from our zipcodes
     *
     * @return void
     */
    public function testGetStateSubdivisionByZipcode()
    {
        $array = array(
            'AU' => array('6798' => 'CX',
                          '6799' => 'CC',
                          '4385' => 'AU-QLD',
                          'NSW 2009' => 'AU-NSW'),
            'CA' => array('A2A 0A0' => 'CA-NL',
                          'A1Y0A0' => 'CA-NL',
                          'b1a0B0' => 'CA-NS',
                          'y0b0a0' => 'CA-YT'),
            'GB' => array('LE9 3BH' => 'GB-LCE',
                          'TN23 4GL' => 'GB-KEN',
                          'G2 4JL' => 'GB-GLG',
                          'IM1 2AR' => 'IM',
                          'SANTA1' => 'GB',
                          'invalid' => NULL),
            'IN' => array('560052' => 'IN-KA',
                          '226018' => 'IN-UP',
                          '109000' => 'IN',
                          '10900' => NULL,
                          '110000' => 'IN-DL',
                          '175000' => 'IN-HP'),
            'NG' => array('105000' => 'NG-LA',
                          '118123' => 'NG',
                          '221987' => 'NG-OS',
                          '231555' => 'NG-OS'),
            'NZ' => array('2145' => 'NZ-AUK',
                          '3178' => 'NZ-BOP',
                          '8024' => 'NZ-CAN',
                          '2112' => 'NZ-AUK'),
            'US' => array('02115' => 'US-MA',
                          '10001' => 'US-NY',
                          '00601' => 'PR',
                          '01001' => 'US-MA')
        );
        /**
         * @var \Bairwell\Zipstates\ZipStates $zipstates
         */
        $zipstates = \Bairwell\ZipStates\Factory::getLibrary('ZipStates');
        foreach ($array as $country => $data) {
            foreach ($data as $zipcode => $expected) {
                $result = $zipstates->getStateSubdivisionByZipcode($country, $zipcode);
                $this->assertEquals($expected, $result, 'When testing ' . $zipcode . ' in ' . $country);
            }
        }
    }

    /**
     * Tests that we get the appropriate countries back from our zipcodes
     *
     * @return void
     */
    public function testGetCountrySubdivisionByZipcode()
    {
        /**
         * @var \Bairwell\Zipstates\ZipStates $zipstates
         */
        $zipstates = \Bairwell\ZipStates\Factory::getLibrary('ZipStates');
        $this->assertEquals('GB-ENG', $zipstates->getCountrySubdivisionByZipcode('GB', 'LE9 3BH'));
        $this->assertEquals('IM', $zipstates->getCountrySubdivisionByZipcode('GB', 'IM12AR'));

    }

    /**
     * Tests that we get the appropriate country back from our zipcode if we don't recognise the country
     *
     * @return void
     */
    public function testGetCountrySubdivisionByZipcodeCountryNotDefined()
    {
        /**
         * @var \Bairwell\Zipstates\ZipStates $zipstates
         */
        $zipstates = \Bairwell\ZipStates\Factory::getLibrary('ZipStates');
        $this->assertEquals('XX', $zipstates->getCountrySubdivisionByZipcode('XX', '1234'));
    }
}